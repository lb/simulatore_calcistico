// Simulatore Calcistico (SC): a football simulator
// Copyright 2024-2025 the SC contributors. See git history for details.
//
// This file is part of Simulatore Calcistico (SC).
//
// SC is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with SC.  If not, see <https://www.gnu.org/licenses/>.

#include <sc/cmdline.hpp>

#include <limits>

#include <doctest/doctest.h>

#include <sc/cmdline_impl.hpp>

CmdLineArg::CmdLineArg(std::initializer_list<std::string_view>&& forms)
	: _forms{ forms }
{
}

auto CmdLineArg::getForms() const -> std::unordered_set<std::string_view> const&
{
	return _forms;
}

auto CmdLineArg::hasForm(std::string_view const form) const -> bool
{
	return _forms.contains(form);
}

CmdLine::CmdLine(usize argc, char const* const* argv)
{
	_argv.reserve(argc);
	for (usize i = 0; i < argc; ++i)
	{
		_argv.emplace_back(argv[i]);
	}
}

CmdLine::CmdLine(std::initializer_list<std::string_view>&& argv)
	: _argv{ argv }
{
}

auto CmdLine::hasFlag(std::string_view const form) const -> bool
{
	return _parsedForms.contains(form);
}

// TESTING AREA ----------------------------------------------------------------

namespace
{

enum class Animal : u8
{
	Cat,
	Dog,
	Bunny,
};

}

namespace cmdline
{

template <>
auto tryParseCustom<Animal>(std::string_view const val) -> std::optional<Animal>
{
	if (val == "cat") return Animal::Cat;
	if (val == "dog") return Animal::Dog;
	if (val == "bunny") return Animal::Bunny;
	return std::nullopt;
}

}

TEST_SUITE("test/cmdline")
{

TEST_CASE("split works")
{
	using Rhs = std::pair<std::string_view, std::string_view>;

	CHECK(split("-x") == Rhs{ "-x", "" });
	CHECK(split("--help") == Rhs{ "--help", "" });
	CHECK(split("-x=4") == Rhs{ "-x", "4" });
	CHECK(split("--x-coord=10") == Rhs{ "--x-coord", "10" });

	CHECK(split("=1234") == Rhs{ "", "1234" });
	CHECK(split("--foo=") == Rhs{ "--foo", "" });
}

TEST_CASE("hasFlag returns true if flag is present")
{
	auto cmdLine = CmdLine{ "sc", "-a", "-b", "--some-flag" };
	cmdLine.parseArgs([](std::string_view const form, std::string_view const val) -> ParseResult {
		ParseArg(void, "-a");
		ParseArg(void, "-b");
		ParseArg(void, "--some-flag");
		return UnknownArg;
	});

	CHECK(cmdLine.hasFlag("-a"));
	CHECK(cmdLine.hasFlag("-b"));
	CHECK(cmdLine.hasFlag("--some-flag"));
	CHECK(!cmdLine.hasFlag("--not-found"));
}

TEST_CASE("hasFlag returns true for parameters with values")
{
	auto cmdLine = CmdLine{ "sc", "-a=10", "-b=hello" };
	cmdLine.parseArgs([](std::string_view const form, std::string_view const val) -> ParseResult {
		ParseArg(i32, "-a");
		ParseArg(std::string_view, "-b");
		return UnknownArg;
	});

	CHECK(cmdLine.hasFlag("-a"));
	CHECK(cmdLine.hasFlag("-b"));
	CHECK(!cmdLine.hasFlag("--not-found"));
}

TEST_CASE("getValue<bool> returns true for flags that are 'on'")
{
	auto cmdLine = CmdLine{ "sc", "-a=0", "-b=on", "--some-flag=yes", "--off=true", "--nope=no" };
	cmdLine.parseArgs([](std::string_view const form, std::string_view const val) -> ParseResult {
		ParseArg(bool, "-a");
		ParseArg(bool, "-b");
		ParseArg(bool, "--some-flag");
		ParseArg(bool, "--off");
		ParseArg(bool, "--nope");
		return UnknownArg;
	});

	CHECK(cmdLine.getValue<bool>("-b") == std::optional{ true });
	CHECK(cmdLine.getValue<bool>("--some-flag") == std::optional{ true });
	CHECK(cmdLine.getValue<bool>("--off") == std::optional{ true });
}

TEST_CASE("getValue<bool> returns false for flags that are 'off'")
{
	auto cmdLine = CmdLine{ "sc", "-a=0", "-b=no", "--some-flag=off", "--off=false" };
	cmdLine.parseArgs([](std::string_view const form, std::string_view const val) -> ParseResult {
		ParseArg(bool, "-a");
		ParseArg(bool, "-b");
		ParseArg(bool, "--some-flag");
		ParseArg(bool, "--off");
		ParseArg(bool, "--nope");
		return UnknownArg;
	});

	CHECK(cmdLine.getValue<bool>("-a") == std::optional{ false });
	CHECK(cmdLine.getValue<bool>("-b") == std::optional{ false });
	CHECK(cmdLine.getValue<bool>("--some-flag") == std::optional{ false });
	CHECK(cmdLine.getValue<bool>("--off") == std::optional{ false });
}

TEST_CASE("getValue<bool> returns nothing for flags that are not bool")
{
	auto cmdLine = CmdLine{ "sc", "-a", "-b=foo", "--some-flag=10" };
	cmdLine.parseArgs([](std::string_view const form, std::string_view const val) -> ParseResult {
		ParseArg(bool, "-a");
		ParseArg(bool, "-b");
		ParseArg(bool, "--some-flag");
		return UnknownArg;
	});

	CHECK(cmdLine.getValue<bool>("-a") == std::nullopt);
	CHECK(cmdLine.getValue<bool>("-b") == std::nullopt);
	CHECK(cmdLine.getValue<bool>("--some-flag") == std::nullopt);
}

TEST_CASE("getValue<u8/i8> works")
{
	auto masterCmdLine = CmdLine{ "sc", "-a=-128", "-b=127", "-c=0", "-d=255", "-e=-129", "-f=256" };

	{
		auto cmdLine = masterCmdLine;
		cmdLine.parseArgs([](std::string_view const form, std::string_view const val) -> ParseResult {
			ParseArg(u8, "-a");
			ParseArg(u8, "-b");
			ParseArg(u8, "-c");
			ParseArg(u8, "-d");
			ParseArg(u8, "-e");
			ParseArg(u8, "-f");
			return UnknownArg;
		});

		CHECK(cmdLine.getValue<u8>("-a") == std::nullopt);
		CHECK(cmdLine.getValue<u8>("-b") == std::optional{ std::numeric_limits<i8>::max() });
		CHECK(cmdLine.getValue<u8>("-c") == std::optional{ 0 });
		CHECK(cmdLine.getValue<u8>("-d") == std::optional{ std::numeric_limits<u8>::max() });
		CHECK(cmdLine.getValue<u8>("-e") == std::nullopt);
		CHECK(cmdLine.getValue<u8>("-f") == std::nullopt);
	}

	{
		auto cmdLine = masterCmdLine;
		cmdLine.parseArgs([](std::string_view const form, std::string_view const val) -> ParseResult {
			ParseArg(i8, "-a");
			ParseArg(i8, "-b");
			ParseArg(i8, "-c");
			ParseArg(i8, "-d");
			ParseArg(i8, "-e");
			ParseArg(i8, "-f");
			return UnknownArg;
		});

		CHECK(cmdLine.getValue<i8>("-a") == std::optional{ std::numeric_limits<i8>::min() });
		CHECK(cmdLine.getValue<i8>("-b") == std::optional{ std::numeric_limits<i8>::max() });
		CHECK(cmdLine.getValue<i8>("-c") == std::optional{ 0 });
		CHECK(cmdLine.getValue<i8>("-d") == std::nullopt);
		CHECK(cmdLine.getValue<i8>("-e") == std::nullopt);
		CHECK(cmdLine.getValue<i8>("-f") == std::nullopt);
	}
}

TEST_CASE("tryParse<u16/i16> works")
{
	auto masterCmdLine = CmdLine{ "sc", "-a=-32768", "-b=32767", "-c=0", "-d=65535", "-e=-32769", "-f=65536" };

	{
		auto cmdLine = masterCmdLine;
		cmdLine.parseArgs([](std::string_view const form, std::string_view const val) -> ParseResult {
			ParseArg(u16, "-a");
			ParseArg(u16, "-b");
			ParseArg(u16, "-c");
			ParseArg(u16, "-d");
			ParseArg(u16, "-e");
			ParseArg(u16, "-f");
			return UnknownArg;
		});

		CHECK(cmdLine.getValue<u16>("-a") == std::nullopt);
		CHECK(cmdLine.getValue<u16>("-b") == std::optional{ std::numeric_limits<i16>::max() });
		CHECK(cmdLine.getValue<u16>("-c") == std::optional{ 0 });
		CHECK(cmdLine.getValue<u16>("-d") == std::optional{ std::numeric_limits<u16>::max() });
		CHECK(cmdLine.getValue<u16>("-e") == std::nullopt);
		CHECK(cmdLine.getValue<u16>("-f") == std::nullopt);
	}

	{
		auto cmdLine = masterCmdLine;
		cmdLine.parseArgs([](std::string_view const form, std::string_view const val) -> ParseResult {
			ParseArg(i16, "-a");
			ParseArg(i16, "-b");
			ParseArg(i16, "-c");
			ParseArg(i16, "-d");
			ParseArg(i16, "-e");
			ParseArg(i16, "-f");
			return UnknownArg;
		});

		CHECK(cmdLine.getValue<i16>("-a") == std::optional{ std::numeric_limits<i16>::min() });
		CHECK(cmdLine.getValue<i16>("-b") == std::optional{ std::numeric_limits<i16>::max() });
		CHECK(cmdLine.getValue<i16>("-c") == std::optional{ 0 });
		CHECK(cmdLine.getValue<i16>("-d") == std::nullopt);
		CHECK(cmdLine.getValue<i16>("-e") == std::nullopt);
		CHECK(cmdLine.getValue<i16>("-f") == std::nullopt);
	}
}

TEST_CASE("tryParse<u32/i32> works")
{
	auto masterCmdLine = CmdLine{
		"sc", //
		"-a=-2147483648",
		"-b=2147483647",
		"-c=0",
		"-d=4294967295",
		"-e=-2147483649",
		"-f=4294967296",
	};

	{
		auto cmdLine = masterCmdLine;
		cmdLine.parseArgs([](std::string_view const form, std::string_view const val) -> ParseResult {
			ParseArg(u32, "-a");
			ParseArg(u32, "-b");
			ParseArg(u32, "-c");
			ParseArg(u32, "-d");
			ParseArg(u32, "-e");
			ParseArg(u32, "-f");
			return UnknownArg;
		});

		CHECK(cmdLine.getValue<u32>("-a") == std::nullopt);
		CHECK(cmdLine.getValue<u32>("-b") == std::optional{ std::numeric_limits<i32>::max() });
		CHECK(cmdLine.getValue<u32>("-c") == std::optional{ 0 });
		CHECK(cmdLine.getValue<u32>("-d") == std::optional{ std::numeric_limits<u32>::max() });
		CHECK(cmdLine.getValue<u32>("-e") == std::nullopt);
		CHECK(cmdLine.getValue<u32>("-f") == std::nullopt);
	}

	{
		auto cmdLine = masterCmdLine;
		cmdLine.parseArgs([](std::string_view const form, std::string_view const val) -> ParseResult {
			ParseArg(i32, "-a");
			ParseArg(i32, "-b");
			ParseArg(i32, "-c");
			ParseArg(i32, "-d");
			ParseArg(i32, "-e");
			ParseArg(i32, "-f");
			return UnknownArg;
		});

		CHECK(cmdLine.getValue<i32>("-a") == std::optional{ std::numeric_limits<i32>::min() });
		CHECK(cmdLine.getValue<i32>("-b") == std::optional{ std::numeric_limits<i32>::max() });
		CHECK(cmdLine.getValue<i32>("-c") == std::optional{ 0 });
		CHECK(cmdLine.getValue<i32>("-d") == std::nullopt);
		CHECK(cmdLine.getValue<i32>("-e") == std::nullopt);
		CHECK(cmdLine.getValue<i32>("-f") == std::nullopt);
	}
}

TEST_CASE("tryParse<u64/i64> works")
{
	auto masterCmdLine = CmdLine{
		"sc",
		"-a=-9223372036854775808",
		"-b=9223372036854775807",
		"-c=0",
		"-d=18446744073709551615",
		"-e=-9223372036854775809",
		"-f=18446744073709551616",
	};

	{
		auto cmdLine = masterCmdLine;
		cmdLine.parseArgs([](std::string_view const form, std::string_view const val) -> ParseResult {
			ParseArg(u64, "-a");
			ParseArg(u64, "-b");
			ParseArg(u64, "-c");
			ParseArg(u64, "-d");
			ParseArg(u64, "-e");
			ParseArg(u64, "-f");
			return UnknownArg;
		});

		CHECK(cmdLine.getValue<u64>("-a") == std::nullopt);
		CHECK(cmdLine.getValue<u64>("-b") == std::optional{ std::numeric_limits<i64>::max() });
		CHECK(cmdLine.getValue<u64>("-c") == std::optional{ 0ull });
		CHECK(cmdLine.getValue<u64>("-d") == std::optional{ std::numeric_limits<u64>::max() });
		CHECK(cmdLine.getValue<u64>("-e") == std::nullopt);
		CHECK(cmdLine.getValue<u64>("-f") == std::nullopt);
	}

	{
		auto cmdLine = masterCmdLine;
		cmdLine.parseArgs([](std::string_view const form, std::string_view const val) -> ParseResult {
			ParseArg(i64, "-a");
			ParseArg(i64, "-b");
			ParseArg(i64, "-c");
			ParseArg(i64, "-d");
			ParseArg(i64, "-e");
			ParseArg(i64, "-f");
			return UnknownArg;
		});

		CHECK(cmdLine.getValue<i64>("-a") == std::optional{ std::numeric_limits<i64>::min() });
		CHECK(cmdLine.getValue<i64>("-b") == std::optional{ std::numeric_limits<i64>::max() });
		CHECK(cmdLine.getValue<i64>("-c") == std::optional{ 0ll });
		CHECK(cmdLine.getValue<i64>("-d") == std::nullopt);
		CHECK(cmdLine.getValue<i64>("-e") == std::nullopt);
		CHECK(cmdLine.getValue<i64>("-f") == std::nullopt);
	}
}

TEST_CASE("tryParse<f32> works")
{
	auto cmdLine = CmdLine{ "sc", "-a=3.14159", "-b=-3.14159", "-c=0", "-d=1.2345e-25", "-e=-1.2345e25" };
	cmdLine.parseArgs([](std::string_view const form, std::string_view const val) -> ParseResult {
		ParseArg(f32, "-a");
		ParseArg(f32, "-b");
		ParseArg(f32, "-c");
		ParseArg(f32, "-d");
		ParseArg(f32, "-e");
		ParseArg(f32, "-f");
		return UnknownArg;
	});

	CHECK(cmdLine.getValue<f32>("-a") == std::optional{ 3.14159f });
	CHECK(cmdLine.getValue<f32>("-b") == std::optional{ -3.14159f });
	CHECK(cmdLine.getValue<f32>("-c") == std::optional{ 0.0f });
	CHECK(cmdLine.getValue<f32>("-d") == std::optional{ 1.2345e-25f });
	CHECK(cmdLine.getValue<f32>("-e") == std::optional{ -1.2345e25f });
}

TEST_CASE("tryParse<f64> works")
{
	auto cmdLine = CmdLine{
		"sc", //
		"-a=3.1415926535897",
		"-b=-3.1415926535897",
		"-c=0",
		"-d=1.23456789e-250",
		"-e=-1.23456789e250",
	};

	cmdLine.parseArgs([](std::string_view const form, std::string_view const val) -> ParseResult {
		ParseArg(f64, "-a");
		ParseArg(f64, "-b");
		ParseArg(f64, "-c");
		ParseArg(f64, "-d");
		ParseArg(f64, "-e");
		ParseArg(f64, "-f");
		return UnknownArg;
	});

	CHECK(cmdLine.getValue<f64>("-a") == std::optional{ 3.1415926535897 });
	CHECK(cmdLine.getValue<f64>("-b") == std::optional{ -3.1415926535897 });
	CHECK(cmdLine.getValue<f64>("-c") == std::optional{ 0.0 });
	CHECK(cmdLine.getValue<f64>("-d") == std::optional{ 1.23456789e-250 });
	CHECK(cmdLine.getValue<f64>("-e") == std::optional{ -1.23456789e250 });
}

TEST_CASE("getValue<std::string_view> works")
{
	using namespace std::literals::string_view_literals;

	auto cmdLine = CmdLine{ "sc", "-a=on", "-b=off", "-c=", "-d=neither", "-e " };
	cmdLine.parseArgs([](std::string_view const form, std::string_view const val) -> ParseResult {
		ParseArg(std::string_view, "-a");
		ParseArg(std::string_view, "-b");
		ParseArg(std::string_view, "-c");
		ParseArg(std::string_view, "-d");
		ParseArg(std::string_view, "-e");
		ParseArg(std::string_view, "-f");
		return UnknownArg;
	});

	CHECK(cmdLine.getValue<std::string_view>("-a") == std::optional{ "on"sv });
	CHECK(cmdLine.getValue<std::string_view>("-b") == std::optional{ "off"sv });
	CHECK(cmdLine.getValue<std::string_view>("-c") == std::nullopt);
	CHECK(cmdLine.getValue<std::string_view>("-d") == std::optional{ "neither"sv });
	CHECK(cmdLine.getValue<std::string_view>("-e") == std::nullopt);
	CHECK(cmdLine.getValue<std::string_view>("--not-found") == std::nullopt);
}

TEST_CASE("multiple forms work")
{
	auto cmdLine = CmdLine{ "sc", "-h", "--version", "-x=42", "--y-coord=21" };
	cmdLine.parseArgs([](std::string_view const form, std::string_view const val) -> ParseResult {
		ParseArg(void, "-h", "--help");
		ParseArg(void, "-v", "--version");
		ParseArg(i32, "-x", "--x-coord");
		ParseArg(i32, "-y", "--y-coord");
		return UnknownArg;
	});

	CHECK(cmdLine.hasFlag("-h"));
	CHECK(cmdLine.hasFlag("--help"));

	CHECK(cmdLine.hasFlag("-v"));
	CHECK(cmdLine.hasFlag("--version"));

	CHECK(cmdLine.getValue<i32>("-x") == std::optional{ 42 });
	CHECK(cmdLine.getValue<i32>("--x-coord") == std::optional{ 42 });

	CHECK(cmdLine.getValue<i32>("-y") == std::optional{ 21 });
	CHECK(cmdLine.getValue<i32>("--y-coord") == std::optional{ 21 });
}

TEST_CASE("doesn't try to parse past the end")
{
	auto cmdLine = CmdLine{ "sc", "-a", "-b=foo", "-c" };
	cmdLine.parseArgs([](std::string_view const form, std::string_view const val) -> ParseResult {
		ParseArg(void, "-a");
		ParseArg(std::string_view, "-b");
		ParseArg(void, "-c");
		return UnknownArg;
	});

	CHECK(cmdLine.hasFlag("-c"));
	CHECK(cmdLine.getValue<u32>("-c") == std::nullopt);
}

TEST_CASE("doesn't try to parse past the end even with malformed flag")
{
	auto cmdLine = CmdLine{ "sc", "-a", "-b=foo", "-c= \t" };
	cmdLine.parseArgs([](std::string_view const form, std::string_view const val) -> ParseResult {
		ParseArg(void, "-a");
		ParseArg(std::string_view, "-b");
		ParseArg(std::string_view, "-c");
		return UnknownArg;
	});

	CHECK(cmdLine.hasFlag("-c"));
	CHECK(cmdLine.getValue<std::string_view>("-c") == std::optional{ " \t" });
}

TEST_CASE("custom parsing works")
{
	auto cmdLine = CmdLine{ "sc", "--first=dog", "--second=bunny" };
	cmdLine.parseArgs([](std::string_view const form, std::string_view const val) -> ParseResult {
		ParseArg(Animal, "-f", "--first");
		ParseArg(Animal, "-s", "--second");
		return UnknownArg;
	});

	CHECK(cmdLine.getValue<Animal>("-f") == Animal::Dog);
	CHECK(cmdLine.getValue<Animal>("-s") == Animal::Bunny);
}

}
