// Simulatore Calcistico (SC): a football simulator
// Copyright 2024-2025 the SC contributors. See git history for details.
//
// This file is part of Simulatore Calcistico (SC).
//
// SC is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with SC.  If not, see <https://www.gnu.org/licenses/>.

#include <sc/std/log.hpp>

#include <string_view>

#include <quill/Backend.h>
#include <quill/Frontend.h>
#include <quill/sinks/ConsoleSink.h>
#include <quill/sinks/RotatingFileSink.h>

#include <sc/cmdline_impl.hpp>

namespace cmdline
{

template <>
auto tryParseCustom<quill::LogLevel>(std::string_view val) -> std::optional<quill::LogLevel>
{
#define ParseLogLevel(Option1, Option2, EnumValue) \
	if (val == (Option1) || val == (Option2)) \
	{ \
		return quill::LogLevel::EnumValue; \
	}

	ParseLogLevel("tracel3", "TraceL3", TraceL3);
	ParseLogLevel("tracel2", "TraceL2", TraceL2);
	ParseLogLevel("tracel1", "TraceL1", TraceL1);
	ParseLogLevel("debug", "Debug", Debug);
	ParseLogLevel("info", "Info", Info);
	ParseLogLevel("notice", "Notice", Notice);
	ParseLogLevel("warning", "Warning", Warning);
	ParseLogLevel("error", "Error", Error);
	ParseLogLevel("critical", "Critical", Critical);
	ParseLogLevel("none", "None", None);

#undef ParseLogLevel

	return std::nullopt;
}

}

namespace sc::log
{

auto getLogger() -> quill::Logger*
{
	return quill::Frontend::get_logger("main");
}

void setupLogging(CmdLine const& cmdLine)
{
	quill::Frontend::preallocate();
	quill::Backend::start<quill::FrontendOptions>(quill::BackendOptions{}, quill::SignalHandlerOptions{});

	auto logPath = cmdLine.getValueOr<std::string_view>("--log-path", "logs/main.log");
	auto fileSink = quill::Frontend::create_or_get_sink<quill::RotatingFileSink>(logPath.data(), [&] {
		auto cfg = quill::RotatingFileSinkConfig{};
		cfg.set_open_mode('w');
		cfg.set_rotation_max_file_size(cmdLine.getValueOr("--log-max-bytesize", 512ull * 1'024ull));
		cfg.set_max_backup_files(cmdLine.getValueOr("--log-max-rotations", 5));

		return cfg;
	}());

	auto consoleSink = quill::Frontend::create_or_get_sink<quill::ConsoleSink>(
		"console-sink",
		quill::ConsoleSink::ColourMode::Always,
		"stderr"
	);

	auto logLevel = cmdLine.getValueOr<quill::LogLevel>("--log-level", quill::LogLevel::Debug);
	fileSink->set_log_level_filter(logLevel);
	consoleSink->set_log_level_filter(logLevel);

	auto* logger = quill::Frontend::create_or_get_logger("main", { std::move(fileSink), std::move(consoleSink) });
	logger->set_log_level(logLevel);
}

}
