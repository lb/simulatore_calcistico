#pragma once

#include <quill/LogMacros.h>
#include <quill/Logger.h>

#include <sc/cmdline.hpp>

namespace cmdline
{

template <>
auto tryParseCustom<quill::LogLevel>(std::string_view val) -> std::optional<quill::LogLevel>;

}

namespace sc::log
{

auto getLogger() -> quill::Logger*;
void setupLogging(CmdLine const& cmdLine);

}

#define LogTraceL3(fmt, ...) QUILL_LOG_TRACE_L3(sc::log::getLogger(), fmt, ##__VA_ARGS__)
#define LogTraceL2(fmt, ...) QUILL_LOG_TRACE_L2(sc::log::getLogger(), fmt, ##__VA_ARGS__)
#define LogTraceL1(fmt, ...) QUILL_LOG_TRACE_L1(sc::log::getLogger(), fmt, ##__VA_ARGS__)
#define LogTrace LogTraceL1
#define LogDebug(fmt, ...) QUILL_LOG_DEBUG(sc::log::getLogger(), fmt, ##__VA_ARGS__)
#define LogInfo(fmt, ...) QUILL_LOG_INFO(sc::log::getLogger(), fmt, ##__VA_ARGS__)
#define LogNotice(fmt, ...) QUILL_LOG_NOTICE(sc::log::getLogger(), fmt, ##__VA_ARGS__)
#define LogWarning(fmt, ...) QUILL_LOG_WARNING(sc::log::getLogger(), fmt, ##__VA_ARGS__)
#define LogError(fmt, ...) QUILL_LOG_ERROR(sc::log::getLogger(), fmt, ##__VA_ARGS__)
#define LogCritical(fmt, ...) QUILL_LOG_CRITICAL(sc::log::getLogger(), fmt, ##__VA_ARGS__)
#define LogBacktrace(fmt, ...) QUILL_LOG_BACKTRACE(sc::log::getLogger(), fmt, ##__VA_ARGS__)
