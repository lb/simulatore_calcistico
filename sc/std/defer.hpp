// Simulatore Calcistico (SC): a football simulator
// Copyright 2024 the SC contributors. See git history for details.
//
// This file is part of Simulatore Calcistico (SC).
//
// SC is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with SC.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include <utility>

#include <sc/std/macros.hpp>

namespace sc
{

template <typename Op>
class DeferredOp
{
	Op _op;

public:
	explicit DeferredOp(Op op)
		: _op{ std::move(op) }
	{
	}

	DeferredOp(DeferredOp const&) = delete;
	DeferredOp(DeferredOp&&) = delete;
	auto operator=(DeferredOp const&) = delete;
	auto operator=(DeferredOp&&) = delete;

	~DeferredOp() { _op(); }
};

}

#define DeferX(VarName, Op) auto const VarName = sc::DeferredOp{ [&] { Op; } };
#define Defer(Op) DeferX(LnName(Deferred_), Op)
