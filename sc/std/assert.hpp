#pragma once

#include <source_location>
#include <string_view>

#include <sc/build_config.hpp>
#include <sc/std/log.hpp>

namespace sc
{

template <typename PredFn, typename... Args>
constexpr void assertImpl(
	PredFn const& predFn,
	std::string_view predStr,
	std::source_location loc = std::source_location::current(),
	std::string_view msg = ""
)
{
	if constexpr (!sc::bcfg::enableAsserts())
	{
		return;
	}

	if (predFn())
	{
		return;
	}

	if (msg == "")
	{
		constexpr auto const* const fmt = "{}:{} in `{}`: Assertion failed: '{}'";
		LogCritical(fmt, loc.file_name(), loc.line(), loc.function_name(), predStr);
		std::abort();
	}

	constexpr auto const* const fmt = "{}:{} in `{}`: Assertion failed with message: '{}' - {}";
	LogCritical(fmt, loc.file_name(), loc.line(), loc.function_name(), predStr, msg);
	std::abort();
}

}

#define Assert(Predicate, ...) \
	sc::assertImpl( \
		[&] { return Predicate; }, \
		#Predicate, \
		std::source_location::current() __VA_OPT__(, std::format(__VA_ARGS__)) \
	);
