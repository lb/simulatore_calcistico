// Simulatore Calcistico (SC): a football simulator
// Copyright 2024-2025 the SC contributors. See git history for details.
//
// This file is part of Simulatore Calcistico (SC).
//
// SC is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with SC.  If not, see <https://www.gnu.org/licenses/>.

#include <string>

#include <SDL3/SDL.h>
#include <doctest/doctest.h>
#include <quill/Frontend.h>

#include <sc/cmdline.hpp>
#include <sc/cmdline_impl.hpp>
#include <sc/rendering/vulkan/vk_renderer.hpp>
#include <sc/std/defer.hpp>
#include <sc/std/fundamental_types.hpp>
#include <sc/std/log.hpp>

namespace
{

auto shouldRunDoctest(CmdLine const& cmdLine) -> bool
{
	return cmdLine.hasFlag("--list-suites") //
		|| cmdLine.hasFlag("--run-tests") //
		|| cmdLine.hasFlag("--run-benchmarks") //
		|| cmdLine.hasFlag("--run-suite");
}

auto runDoctest(CmdLine const& cmdLine) -> i32
{
	doctest::Context ctx;

	// If we run doctest, also quit after
	ctx.setOption("dt-no-run", false);
	ctx.setOption("dt-exit", true);

	bool const runTests = cmdLine.hasFlag("--run-tests");
	bool const runBenchmarks = cmdLine.hasFlag("--run-benchmarks");
	bool const runSuite = cmdLine.hasFlag("--run-suite");

	if (runSuite)
	{
		auto const suite = cmdLine.getValue<std::string_view>("--run-suite");
		ctx.setOption("dt-test-suite", std::string{ *suite }.c_str());
	}
	else if (runTests && runBenchmarks)
	{
		ctx.setOption("dt-test-suite", "test*,bench*");
	}
	else if (runTests)
	{
		ctx.setOption("dt-test-suite", "test*");
	}
	else if (runBenchmarks)
	{
		ctx.setOption("dt-test-suite", "bench*");
	}

	// unlike options, flags like dt-list-test-suites cannot be set programmatically,
	// they need to be passed via command line. So we need to create a faux one
	std::vector<char const*> doctestFauxCmdLine;
	if (cmdLine.hasFlag("--list-suites"))
	{
		doctestFauxCmdLine.emplace_back("--dt-list-test-suites");
	}

	ctx.applyCommandLine(static_cast<i32>(doctestFauxCmdLine.size()), doctestFauxCmdLine.data());
	return ctx.run();
}

auto runSc([[maybe_unused]] CmdLine const& cmdLine) -> i32
{
	SDL_Init(SDL_INIT_VIDEO);
	Defer(SDL_Quit());

	auto* window = SDL_CreateWindow("Simulatore Calcistico", 800, 600, SDL_WINDOW_VULKAN);
	Defer(SDL_DestroyWindow(window));

	if (window == nullptr)
	{
		LogError("Could not create window: {}", SDL_GetError());
		return 1;
	}

	auto vkRenderer = sc::VkRenderer{ window };

	auto event = SDL_Event{};
	for (bool closeWindow = false; !closeWindow;)
	{
		while (SDL_PollEvent(&event))
		{
			switch (event.type)
			{
			case SDL_EVENT_QUIT:
				closeWindow = true;
				break;

			default:
				break;
			}
		}

		vkRenderer.drawFrame();
	}

	return 0;
}

}

auto main(i32 argc, char* argv[]) -> i32
{
	auto cmdLine = CmdLine{ static_cast<usize>(argc), argv };
	cmdLine.parseArgs([](std::string_view const form, std::string_view const val) -> ParseResult {
		ParseArg(void, "-l", "--list-suites");
		ParseArg(void, "-t", "--run-tests");
		ParseArg(void, "-b", "--run-benchmarks");
		ParseArg(std::string_view, "-s", "--run-suite");

		ParseArg(std::string_view, "--log-path");
		ParseArg(usize, "--log-max-bytesize");
		ParseArg(u32, "--log-max-rotations");
		ParseArg(quill::LogLevel, "--log-level");

		return UnknownArg;
	});

	if (shouldRunDoctest(cmdLine))
	{
		return runDoctest(cmdLine);
	}

	sc::log::setupLogging(cmdLine);
	LogInfo("Logger setup successful");

	return runSc(cmdLine);
}
