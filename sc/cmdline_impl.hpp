// Simulatore Calcistico (SC): a football simulator
// Copyright 2024-2025 the SC contributors. See git history for details.
//
// This file is part of Simulatore Calcistico (SC).
//
// SC is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with SC.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include <sc/cmdline.hpp>

#include <charconv>
#include <cstdlib>
#include <cstring>
#include <functional>

namespace
{

constexpr auto split(std::string_view const rawArg) -> std::pair<std::string_view, std::string_view>
{
	if (rawArg.empty())
	{
		return {};
	}

	auto const pos = rawArg.find('=');
	if (pos == std::string_view::npos)
	{
		// could be a flag like --help, treat entire rawArg as a form
		return { rawArg, {} };
	}

	if (pos == rawArg.size() - 1)
	{
		// mistyped, could be something like "--foo=" (excluding quotes). Treat as if "--foo"
		return { rawArg.substr(0, pos), {} };
	}

	return { rawArg.substr(0, pos), rawArg.substr(pos + 1) };
}

}

template <typename T>
	requires(std::is_integral_v<T> // || std::is_floating_point_v<T> when libc++ implements FP from_chars
             && !std::is_same_v<T, bool>)
auto tryParse(std::string_view const val) -> std::optional<T>
{
	auto parsed = T{};
	auto result = std::from_chars(val.data(), val.data() + val.size(), parsed);

	if (result.ec != std::errc{})
	{
		return std::nullopt;
	}

	return std::optional<T>{ parsed };
}

template <typename T>
	requires std::is_floating_point_v<T>
auto tryParse(std::string_view const val) -> std::optional<T>
{
	auto parseFp = [](char const* str, char** strEnd) {
		if constexpr (std::is_same_v<T, f32>)
		{
			return std::strtof(str, strEnd);
		}

		if constexpr (std::is_same_v<T, f64>)
		{
			return std::strtod(str, strEnd);
		}
		else
		{
			*strEnd = const_cast<char*>(str);
			return std::numeric_limits<T>::infinity();
		}
	};

	char* end = nullptr;
	auto parsed = parseFp(val.data(), &end);

	if (end == val.data())
	{
		// no conversion could be performed
		return std::nullopt;
	}

	if (parsed == std::numeric_limits<T>::infinity())
	{
		// converted value out of range
		return std::nullopt;
	}

	return std::optional<T>{ parsed };
}

template <typename T>
	requires std::is_same_v<T, bool>
auto tryParse(std::string_view const val) -> std::optional<T>
{
	if (val == "1" || val == "on" || val == "yes" || val == "true")
	{
		return std::optional{ true };
	}

	if (val == "0" || val == "no" || val == "off" || val == "false")
	{
		return std::optional{ false };
	}

	return std::nullopt;
}

template <typename T>
	requires std::is_same_v<T, std::string_view>
auto tryParse(std::string_view const val) -> std::optional<T>
{
	if (val.size() == 0)
	{
		return std::nullopt;
	}

	if (val[0] == '\0')
	{
		return std::nullopt;
	}

	return std::optional{ val };
}

// Declare (but don't define) a tryParse<void> to allow ParseArg(void, "-x") to compile
template <typename T>
	requires std::is_same_v<T, void>
auto tryParse(std::string_view) -> std::optional<std::nullptr_t>;

template <typename T>
	requires(!(std::is_integral_v<T> && !std::is_same_v<T, bool>) //
		&& !std::is_floating_point_v<T>
		&& !std::is_same_v<T, bool>
		&& !std::is_same_v<T, std::string_view>
		&& !std::is_same_v<T, void>)
auto tryParse(std::string_view const val) -> std::optional<T>
{
	using namespace cmdline;
	return tryParseCustom<T>(val);
}

template <typename Func>
void CmdLine::parseArgs(Func&& func)
{
	for (auto const rawArg : _argv)
	{
		auto const [form, strVal] = split(rawArg);
		auto const& [success, parsedVal, typeSafeArg] = std::invoke(std::forward<Func>(func), form, strVal);
		if (!success)
		{
			continue;
		}

		for (auto const form : typeSafeArg.getForms())
		{
			_parsedForms[form] = parsedVal;
		}
	};
}

template <typename T>
	requires(!std::is_same_v<T, void>)
auto CmdLine::getValue(std::string_view const form) const -> std::optional<T>
{
	auto const entry = _parsedForms.find(form);
	if (entry == _parsedForms.cend())
	{
		return std::nullopt;
	}

	std::any const anyVal = entry->second;
	std::optional<T> const* innerVal = std::any_cast<std::optional<T>>(&anyVal);
	if (!innerVal)
	{
		return std::nullopt;
	}

	return *innerVal;
}

template <typename T>
	requires(!std::is_same_v<T, void>)
auto CmdLine::getValueOr(std::string_view const form, T&& defVal) const -> T
{
	auto const entry = _parsedForms.find(form);
	if (entry == _parsedForms.cend())
	{
		return std::forward<T>(defVal);
	}

	std::any const anyVal = entry->second;
	std::optional<T> const* innerVal = std::any_cast<std::optional<T>>(&anyVal);
	if (!innerVal)
	{
		return std::forward<T>(defVal);
	}

	return innerVal->value_or(std::forward<T>(defVal));
}
