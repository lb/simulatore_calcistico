// Simulatore Calcistico (SC): a football simulator
// Copyright 2024 the SC contributors. See git history for details.
//
// This file is part of Simulatore Calcistico (SC).
//
// SC is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with SC.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

namespace sc::bcfg
{

consteval auto enableValidationLayers() -> bool
{
#if defined(SC_ENABLE_VULKAN_VALIDATION_LAYERS)
	return true;
#else
	return false;
#endif
}

consteval auto enableAsserts() -> bool
{
#if defined(SC_ENABLE_ASSERTS)
	return true;
#else
	return false;
#endif
}

}
