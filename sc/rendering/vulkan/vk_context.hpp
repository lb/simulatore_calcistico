// Simulatore Calcistico (SC): a football simulator
// Copyright 2024-2025 the SC contributors. See git history for details.
//
// This file is part of Simulatore Calcistico (SC).
//
// SC is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with SC.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include <vector>

#include <vulkan/vulkan.hpp>

#include <sc/std/assert.hpp>
#include <sc/std/fundamental_types.hpp>

struct SDL_Window;

#define VkAssert(R, Msg, ...) \
	Assert((R) == vk::Result::eSuccess, Msg ": {}", std::to_underlying(R) __VA_OPT__(, ) __VA_ARGS__);

namespace sc
{

// contains the index of the first queue family that supports a feature or
// requirement, or std::nullopt if no such family is found
struct QueueFamilyIndices
{
	std::optional<u32> gfx;
	std::optional<u32> present;

	[[nodiscard]] auto foundAll() const -> bool { return gfx.has_value() && present.has_value(); }
};

class VkContext
{
	SDL_Window* _window;

	vk::Instance _instance;
	vk::DebugUtilsMessengerEXT _dbgMessenger;
	vk::SurfaceKHR _surface;
	vk::PhysicalDevice _physicalDevice;
	vk::Device _logicalDevice;
	vk::Queue _gfxQueue;
	vk::Queue _presentQueue;
	vk::SwapchainKHR _swapchain;

	std::vector<vk::Image> _swapchainImages;
	std::vector<vk::ImageView> _imageViews;
	vk::Format _imageFormat = vk::Format::eUndefined;
	vk::Extent2D _imageExtent;

	QueueFamilyIndices _queueFamilyIndices;

	void createInstance();
	void createDbgMessenger();
	void createSurface();
	void selectPhysicalDevice();
	void createLogicalDevice();
	void createSwapchain();
	void createImageViews();

public:
	friend class VkRenderer;

	VkContext(VkContext const&) = delete;
	VkContext(VkContext&&) = delete;
	auto operator=(VkContext&&) = delete;
	auto operator=(VkContext const&) = delete;

	VkContext(SDL_Window* window);
	~VkContext();
};

}
