// Simulatore Calcistico (SC): a football simulator
// Copyright 2025 the SC contributors. See git history for details.
//
// This file is part of Simulatore Calcistico (SC).
//
// SC is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with SC.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include <array>

#include <sc/rendering/vulkan/vk_context.hpp>
#include <sc/std/fundamental_types.hpp>

namespace sc
{

class VkRenderer
{
	constexpr static usize const frameBacklogSize = 2ull;

	struct FrameData
	{
		vk::CommandPool commandPool;
		vk::CommandBuffer commandBuffer;
		vk::Fence renderFence;
		vk::Semaphore swapchainSemaphore;
		vk::Semaphore renderSemaphore;
	};

	VkContext _vkContext;
	std::array<FrameData, frameBacklogSize> _frameData;
	usize _presentedFrames = 0ull;

	void createCommandPools();
	void allocateCommandBuffers();
	void createSynchronizationPrimitives();

	auto getFrame() -> FrameData&;

public:
	VkRenderer(VkRenderer const&) = delete;
	VkRenderer(VkRenderer&&) = delete;
	auto operator=(VkRenderer const&) = delete;
	auto operator=(VkRenderer&&) = delete;

	VkRenderer(SDL_Window* window);
	~VkRenderer();

	void drawFrame();
};

}
