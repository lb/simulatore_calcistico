// Simulatore Calcistico (SC): a football simulator
// Copyright 2025 the SC contributors. See git history for details.
//
// This file is part of Simulatore Calcistico (SC).
//
// SC is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with SC.  If not, see <https://www.gnu.org/licenses/>.

#include <cmath>

#include <sc/rendering/vulkan/vk_renderer.hpp>

namespace
{

void transitionImage(vk::Image img, vk::CommandBuffer buf, vk::ImageLayout oldLayout, vk::ImageLayout newLayout)
{
	auto imgBarrier = vk::ImageMemoryBarrier2 {
		.srcStageMask = vk::PipelineStageFlagBits2::eAllCommands,
		.srcAccessMask = vk::AccessFlagBits2::eMemoryWrite,
		.dstStageMask = vk::PipelineStageFlagBits2::eAllCommands,
		.dstAccessMask = vk::AccessFlagBits2::eMemoryWrite | vk::AccessFlagBits2::eMemoryRead,
		.oldLayout = oldLayout,
		.newLayout = newLayout,
		.image = img,
		.subresourceRange = {
			.aspectMask = (newLayout == vk::ImageLayout::eAttachmentOptimal)
				? vk::ImageAspectFlagBits::eDepth
				: vk::ImageAspectFlagBits::eColor,
			.baseMipLevel = 0,
			.levelCount = vk::RemainingMipLevels,
			.baseArrayLayer = 0,
			.layerCount = vk::RemainingArrayLayers,
		},
	};

	buf.pipelineBarrier2({
		.imageMemoryBarrierCount = 1,
		.pImageMemoryBarriers = &imgBarrier,
	});
}

}

namespace sc
{

void VkRenderer::createCommandPools()
{
	auto const poolCreateInfo = vk::CommandPoolCreateInfo{
		.flags = vk::CommandPoolCreateFlagBits::eResetCommandBuffer,
		.queueFamilyIndex = _vkContext._queueFamilyIndices.gfx.value(),
	};

	for (auto& frame : _frameData)
	{
		vk::Result result = {};
		std::tie(result, frame.commandPool) = _vkContext._logicalDevice.createCommandPool(poolCreateInfo);
		VkAssert(result, "could not create command pool");
	}

	LogInfo("vk::CommandPool (x{}) created successfully", frameBacklogSize);
}

void VkRenderer::allocateCommandBuffers()
{
	for (auto& frame : _frameData)
	{
		auto const [result, buffers] = _vkContext._logicalDevice.allocateCommandBuffers({
			.commandPool = frame.commandPool,
			.level = vk::CommandBufferLevel::ePrimary,
			.commandBufferCount = 1,
		});

		VkAssert(result, "could not create command buffer");
		frame.commandBuffer = buffers[0];
	}

	LogInfo("vk::CommandBuffer (x{}) created successfully", frameBacklogSize);
}

void VkRenderer::createSynchronizationPrimitives()
{
	vk::Result result = {};

	auto device = _vkContext._logicalDevice;
	for (auto& frame : _frameData)
	{
		std::tie(result, frame.renderFence) = device.createFence({
			.flags = vk::FenceCreateFlagBits::eSignaled,
		});
		VkAssert(result, "could not create render fence");

		std::tie(result, frame.swapchainSemaphore) = device.createSemaphore({});
		VkAssert(result, "could not create swapchain semaphore");

		std::tie(result, frame.renderSemaphore) = device.createSemaphore({});
		VkAssert(result, "could not create render semaphore");
	}

	LogInfo("vk::Fence(render) (x{}) created successfully", frameBacklogSize);
	LogInfo("vk::Semaphore(swapchain) (x{}) created successfully", frameBacklogSize);
	LogInfo("vk::Semaphore(render) (x{}) created successfully", frameBacklogSize);
}

auto VkRenderer::getFrame() -> FrameData&
{
	auto const frameDataIdx = _presentedFrames % frameBacklogSize;
	return _frameData[frameDataIdx];
}

VkRenderer::VkRenderer(SDL_Window* window)
	: _vkContext{ window }
{
	createCommandPools();
	allocateCommandBuffers();
	createSynchronizationPrimitives();
}

VkRenderer::~VkRenderer()
{
	auto device = _vkContext._logicalDevice;
	auto result = device.waitIdle();
	VkAssert(result, "could not wait for logical device to become idle");

	for (auto& frame : _frameData)
	{
		device.destroyCommandPool(frame.commandPool);
		device.destroySemaphore(frame.renderSemaphore);
		device.destroySemaphore(frame.swapchainSemaphore);
		device.destroyFence(frame.renderFence);
	}
}

void VkRenderer::drawFrame()
{
	auto frame = getFrame();

	auto device = _vkContext._logicalDevice;
	u64 timeout = 1'000'000'000; // 1 second

	// handle fence
	auto result = device.waitForFences(1, &frame.renderFence, vk::True, timeout);
	VkAssert(result, "waiting for render fence timed out");

	result = device.resetFences(1, &frame.renderFence);
	VkAssert(result, "could not reset render fence");

	// acquire image
	u32 imageIdx = 0u;
	std::tie(result, imageIdx) = device.acquireNextImageKHR(_vkContext._swapchain, timeout, frame.swapchainSemaphore);
	VkAssert(result, "could not acquire swapchain image");

	// start command buffer
	auto commandBuffer = frame.commandBuffer;
	result = commandBuffer.reset();
	VkAssert(result, "could not reset command buffer");

	result = commandBuffer.begin({ .flags = vk::CommandBufferUsageFlagBits::eOneTimeSubmit });
	VkAssert(result, "could not begin command buffer");

	// clear image
	auto img = _vkContext._swapchainImages[imageIdx];
	transitionImage(img, commandBuffer, vk::ImageLayout::eUndefined, vk::ImageLayout::eGeneral);

	commandBuffer.clearColorImage(
		img,
		vk::ImageLayout::eGeneral,
		vk::ClearColorValue{
			.float32 = std::array{ .0f, .0f, std::abs(std::sin(_presentedFrames / 120.0f)), .0f },
		},
		vk::ImageSubresourceRange{
			.aspectMask = vk::ImageAspectFlagBits::eColor,
			.baseMipLevel = 0,
			.levelCount = vk::RemainingMipLevels,
			.baseArrayLayer = 0,
			.layerCount = vk::RemainingArrayLayers,
		}
	);

	transitionImage(img, commandBuffer, vk::ImageLayout::eGeneral, vk::ImageLayout::ePresentSrcKHR);

	// end command buffer
	result = commandBuffer.end();
	VkAssert(result, "could not end command buffer");

	// submit command buffer
	auto waitSemaphoreInfo = vk::SemaphoreSubmitInfo{
		.semaphore = frame.swapchainSemaphore,
		.value = 1,
		.stageMask = vk::PipelineStageFlagBits2::eColorAttachmentOutput,
		.deviceIndex = 0,
	};

	auto signalSemaphoreInfo = vk::SemaphoreSubmitInfo{
		.semaphore = frame.renderSemaphore,
		.value = 1,
		.stageMask = vk::PipelineStageFlagBits2::eAllGraphics,
		.deviceIndex = 0,
	};

	auto commandBufferInfo = vk::CommandBufferSubmitInfo{
		.commandBuffer = commandBuffer,
		.deviceMask = 0,
	};

	result = _vkContext._gfxQueue.submit2(
		vk::SubmitInfo2{
			.waitSemaphoreInfoCount = 1,
			.pWaitSemaphoreInfos = &waitSemaphoreInfo,
			.commandBufferInfoCount = 1,
			.pCommandBufferInfos = &commandBufferInfo,
			.signalSemaphoreInfoCount = 1,
			.pSignalSemaphoreInfos = &signalSemaphoreInfo,
		},
		frame.renderFence
	);

	VkAssert(result, "could not submit commands to gfx queue");
	// present
	result = _vkContext._presentQueue.presentKHR({
		.waitSemaphoreCount = 1,
		.pWaitSemaphores = &frame.renderSemaphore,
		.swapchainCount = 1,
		.pSwapchains = &_vkContext._swapchain,
		.pImageIndices = &imageIdx,
	});
	VkAssert(result, "could not present with present queue");

	// frame presented, keep tally of it
	++_presentedFrames;
}

}
