// Simulatore Calcistico (SC): a football simulator
// Copyright 2024-2025 the SC contributors. See git history for details.
//
// This file is part of Simulatore Calcistico (SC).
//
// SC is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with SC.  If not, see <https://www.gnu.org/licenses/>.

#include <sc/rendering/vulkan/vk_context.hpp>

#include <algorithm>
#include <limits>
#include <optional>
#include <ranges>
#include <utility>

#include <SDL3/SDL_video.h>
#include <SDL3/SDL_vulkan.h>
#include <quill/std/Vector.h>

#include <sc/build_config.hpp>
#include <sc/std/fundamental_types.hpp>
#include <sc/std/log.hpp>

VULKAN_HPP_DEFAULT_DISPATCH_LOADER_DYNAMIC_STORAGE;

namespace
{

struct SwapchainProperties
{
	vk::SurfaceCapabilitiesKHR capabilities;
	std::vector<vk::SurfaceFormatKHR> formats;
	std::vector<vk::PresentModeKHR> presentModes;
};

}

namespace
{

VKAPI_ATTR auto VKAPI_CALL dbgCallback(
	[[maybe_unused]] vk::DebugUtilsMessageSeverityFlagBitsEXT Severity,
	[[maybe_unused]] vk::DebugUtilsMessageTypeFlagsEXT msgType,
	vk::DebugUtilsMessengerCallbackDataEXT const* data,
	[[maybe_unused]] void* userData
) -> vk::Bool32
{
	LogDebug("layer msg: {}", data->pMessage);
	return vk::False;
}

constexpr auto getDbgMessengerCreateInfo() -> vk::DebugUtilsMessengerCreateInfoEXT
{
	if (!sc::bcfg::enableValidationLayers())
	{
		return vk::DebugUtilsMessengerCreateInfoEXT{};
	}

	using SeverityFlags = vk::DebugUtilsMessageSeverityFlagBitsEXT;
	using TypeFlags = vk::DebugUtilsMessageTypeFlagBitsEXT;

	return vk::DebugUtilsMessengerCreateInfoEXT{
		.messageSeverity = SeverityFlags::eVerbose | SeverityFlags::eWarning | SeverityFlags::eError,
		.messageType = TypeFlags::eGeneral | TypeFlags::eValidation | TypeFlags::ePerformance,
		.pfnUserCallback = dbgCallback,
		.pUserData = nullptr,
	};
}

auto getWantedLayers() -> std::vector<char const*>
{
	auto wantedLayers = std::vector<char const*>{};

	if (sc::bcfg::enableValidationLayers())
	{
		wantedLayers.push_back("VK_LAYER_KHRONOS_validation");
	}

	LogTrace("wanted layers: {}", wantedLayers);
	return wantedLayers;
}

auto getWantedInstanceExtensions() -> std::vector<char const*>
{
	auto wantedExtensions = [] {
		auto numExtensions = 0u;
		auto const* const* extensions = SDL_Vulkan_GetInstanceExtensions(&numExtensions);

		return std::vector<char const*>{ extensions, extensions + numExtensions };
	}();

	if (sc::bcfg::enableValidationLayers())
	{
		wantedExtensions.push_back(vk::EXTDebugUtilsExtensionName);
	}

	LogTrace("wanted instance extensions: {}", wantedExtensions);
	return wantedExtensions;
}

auto getWantedPhysicalDeviceExtensions() -> std::vector<char const*>
{
	auto wantedExtensions = std::vector<char const*>{
		vk::KHRSwapchainExtensionName,
	};

	LogTrace("wanted physical device extensions: {}", wantedExtensions);
	return wantedExtensions;
}

auto findQueueFamilyIndices(vk::PhysicalDevice const& device, vk::SurfaceKHR const& surface) -> sc::QueueFamilyIndices
{
	auto const suitableIf = [&](std::optional<u32>& outIdx, u32 const currIdx, bool const pred, char const* kind) {
		if (outIdx.has_value())
		{
			return;
		}

		if (!pred)
		{
			return;
		}

		outIdx = currIdx;
		LogTrace("found suitable {} queue family index: {}", kind, currIdx);
	};

	auto f = sc::QueueFamilyIndices{};
	auto const propsArr = device.getQueueFamilyProperties();
	for (usize idx = 0u; idx < propsArr.size(); ++idx)
	{
		auto const& props = propsArr[idx];
		suitableIf(f.gfx, idx, static_cast<bool>(props.queueFlags & vk::QueueFlagBits::eGraphics), "graphics");
		suitableIf(f.present, idx, device.getSurfaceSupportKHR(idx, surface).result == vk::Result::eSuccess, "present");
	}

	return f;
}

auto getSwapchainProperties(vk::PhysicalDevice const& device, vk::SurfaceKHR const& surface) -> SwapchainProperties
{
	auto props = SwapchainProperties{};
	auto result = vk::Result{};

	[[maybe_unused]] auto const* deviceName = device.getProperties().deviceName.data();
	std::tie(result, props.capabilities) = device.getSurfaceCapabilitiesKHR(surface);
	VkAssert(result, "could not get surface capabilities for physical device {}", deviceName);

	std::tie(result, props.formats) = device.getSurfaceFormatsKHR(surface);
	VkAssert(result, "could not get surface formats for physical device {}", deviceName);

	std::tie(result, props.presentModes) = device.getSurfacePresentModesKHR(surface);
	VkAssert(result, "could not get surface present modes for physical device {}", deviceName);

	return props;
}

auto chooseSurfaceFormat(std::vector<vk::SurfaceFormatKHR> const& availableFormats) -> vk::SurfaceFormatKHR
{
	Assert(!availableFormats.empty());

	auto const preferences = std::array{
		vk::SurfaceFormatKHR{ .format = vk::Format::eB8G8R8A8Srgb, .colorSpace = vk::ColorSpaceKHR::eSrgbNonlinear },
	};

	for (auto const& availableFormat : availableFormats)
	{
		auto const* found = std::ranges::find(preferences, availableFormat);
		if (found == std::ranges::end(preferences))
		{
			continue;
		}

		return *found;
	}

	LogWarning("could not choose preferred swapchain format, using first available");
	return availableFormats[0];
}

auto chooseSwapchainPresentMode(std::vector<vk::PresentModeKHR> const& availablePresentModes) -> vk::PresentModeKHR
{
	Assert(!availablePresentModes.empty());

	auto const preferences = std::array{
		vk::PresentModeKHR::eMailbox,
		vk::PresentModeKHR::eFifo,
	};

	for (auto const& availableMode : availablePresentModes)
	{
		auto const* found = std::ranges::find(preferences, availableMode);
		if (found == std::ranges::end(preferences))
		{
			continue;
		}

		return *found;
	}

	LogWarning("could not choose preferred swapchain present mode, using first available");
	return availablePresentModes[0];
}

auto chooseImageExtent(vk::SurfaceCapabilitiesKHR const& capabilities, SDL_Window* window) -> vk::Extent2D
{
	if (capabilities.currentExtent.width != std::numeric_limits<u32>::max())
	{
		return capabilities.currentExtent;
	}

	i32 width = 0;
	i32 height = 0;
	bool success = SDL_GetWindowSize(window, &width, &height);
	if (!success)
	{
		LogError("could not retrieve window size: {}", SDL_GetError());
		return {};
	}

	auto const& minExtent = capabilities.minImageExtent;
	auto const& maxExtent = capabilities.maxImageExtent;

	return {
		.width = std::clamp(static_cast<u32>(width), minExtent.width, maxExtent.width),
		.height = std::clamp(static_cast<u32>(height), minExtent.height, maxExtent.height),
	};
}

auto canEnableLayers(std::ranges::input_range auto&& wantedLayers) -> bool
{
	if (!sc::bcfg::enableValidationLayers())
	{
		return false;
	}

	auto const [result, availableLayers] = vk::enumerateInstanceLayerProperties();
	VkAssert(result, "could not enumerate instance layer properties");

	auto const isAvailable = [&](auto const& wantedLayer) {
		auto const haveSameName = [](auto const& wantedLayer, auto const& availableLayer) {
			return std::strcmp(wantedLayer, availableLayer.layerName.data()) == 0;
		};

		auto const found = std::ranges::any_of(availableLayers, [&](auto const& availableLayer) {
			return haveSameName(wantedLayer, availableLayer);
		});

		if (!found)
		{
			LogError("layer {} is not available", wantedLayer);
		}

		return found;
	};

	return std::ranges::all_of(wantedLayers, isAvailable);
}

auto canSelectPhysicalDevice(
	vk::PhysicalDevice const& device,
	vk::SurfaceKHR const& surface,
	std::vector<char const*> const& wantedExtensions
) -> bool
{
	auto const [result, availableExtensions] = device.enumerateDeviceExtensionProperties();
	VkAssert(result, "could not enumrate physical device extension properties");

	auto const isExtensionAvailable = [&](auto const& wantedExtension) {
		auto const haveSameName = [](auto const& wantedExtension, auto const& availableExtension) {
			return std::strcmp(wantedExtension, availableExtension.extensionName.data()) == 0;
		};

		auto const found = std::ranges::any_of(availableExtensions, [&](auto const& availableExtension) {
			return haveSameName(wantedExtension, availableExtension);
		});

		if (!found)
		{
			LogTrace(
				"extension {} is not available for physical device {}",
				wantedExtension,
				device.getProperties().deviceName.data()
			);
		}

		return found;
	};

	if (!std::ranges::all_of(wantedExtensions, isExtensionAvailable))
	{
		return false;
	}

	auto const swapchainProps = getSwapchainProperties(device, surface);
	if (swapchainProps.formats.empty() || swapchainProps.presentModes.empty())
	{
		return false;
	}

	auto const features = device.getFeatures2<
		vk::PhysicalDeviceFeatures2,
		vk::PhysicalDeviceVulkan12Features,
		vk::PhysicalDeviceVulkan13Features>();

	return features.get<vk::PhysicalDeviceVulkan12Features>().bufferDeviceAddress == vk::True
		&& features.get<vk::PhysicalDeviceVulkan12Features>().descriptorIndexing == vk::True
		&& features.get<vk::PhysicalDeviceVulkan13Features>().dynamicRendering == vk::True
		&& features.get<vk::PhysicalDeviceVulkan13Features>().synchronization2 == vk::True;
}

}

namespace sc
{

void VkContext::createInstance()
{
	auto const appInfo = vk::ApplicationInfo{
		.pApplicationName = "Simulatore Calcistico",
		.applicationVersion = vk::makeApiVersion(0, 0, 0, 1),
		.pEngineName = "No Engine",
		.engineVersion = vk::makeApiVersion(0, 0, 0, 0),
		.apiVersion = vk::ApiVersion13,
	};

	auto const wantedLayers = getWantedLayers();
	auto const wantedExtensions = getWantedInstanceExtensions();
	auto const dbgMessengerCreateInfo = getDbgMessengerCreateInfo();

	bool const enableLayers = canEnableLayers(wantedLayers);
	auto const instanceCreateInfo = vk::InstanceCreateInfo{
		.pNext = (sc::bcfg::enableValidationLayers()) ? &dbgMessengerCreateInfo : nullptr,
		.flags = {},
		.pApplicationInfo = &appInfo,
		.enabledLayerCount = (enableLayers) ? static_cast<u32>(wantedLayers.size()) : 0u,
		.ppEnabledLayerNames = (enableLayers) ? wantedLayers.data() : nullptr,
		.enabledExtensionCount = static_cast<u32>(wantedExtensions.size()),
		.ppEnabledExtensionNames = wantedExtensions.data(),
	};

	auto result = vk::Result{};
	std::tie(result, _instance) = vk::createInstance(instanceCreateInfo);
	VkAssert(result, "could not create instace");

	LogTrace("vk::Instance created successfully");
}

void VkContext::createDbgMessenger()
{
	if (!sc::bcfg::enableValidationLayers())
	{
		return;
	}

	auto const createInfo = getDbgMessengerCreateInfo();

	auto result = vk::Result{};
	std::tie(result, _dbgMessenger) = _instance.createDebugUtilsMessengerEXT(createInfo);
	VkAssert(result, "could not create debug messenger");

	LogTrace("vk::DebugUtilsMessengerEXT created successfully");
}

void VkContext::createSurface()
{
	VkSurfaceKHR surface = {};
	auto success = SDL_Vulkan_CreateSurface(_window, _instance, nullptr, &surface);

	if (!success)
	{
		LogError("could not create vulkan surface: {}", SDL_GetError());
		return;
	}

	_surface = surface;
	LogTrace("vk::SurfaceKHR created successfully");
}

void VkContext::selectPhysicalDevice()
{
	auto const [result, physicalDevices] = _instance.enumeratePhysicalDevices();
	VkAssert(result, "could not enumerate physical devices");

	for (auto const& device : physicalDevices)
	{
		auto const queueFamilyIndices = findQueueFamilyIndices(device, _surface);
		if (!queueFamilyIndices.foundAll())
		{
			continue;
		}

		if (!canSelectPhysicalDevice(device, _surface, getWantedPhysicalDeviceExtensions()))
		{
			continue;
		}

		_physicalDevice = device;
		_queueFamilyIndices = queueFamilyIndices;
		LogInfo("vk::PhysicalDevice selected: {}", _physicalDevice.getProperties().deviceName.data());

		return;
	}

	LogError("could not find usable physical device");
}

void VkContext::createLogicalDevice()
{
	Assert(_queueFamilyIndices.foundAll());

	auto const queuePrio = 1.0f;
	auto uniqueFamilyIndices = std::unordered_set<std::optional<u32>>{
		{ _queueFamilyIndices.gfx },
		{ _queueFamilyIndices.present },
	};

	auto queueCreateInfos = std::vector<vk::DeviceQueueCreateInfo>{};
	queueCreateInfos.reserve(uniqueFamilyIndices.size());

	for (auto const& familyIndex : uniqueFamilyIndices)
	{
		queueCreateInfos.push_back(vk::DeviceQueueCreateInfo{
			.queueFamilyIndex = familyIndex.value(),
			.queueCount = 1,
			.pQueuePriorities = &queuePrio,
		});
	}

	auto const wantedExtensions = getWantedPhysicalDeviceExtensions();

	vk::StructureChain createInfoChain = {
		vk::DeviceCreateInfo{
			.queueCreateInfoCount = 1,
			.pQueueCreateInfos = queueCreateInfos.data(),
			.enabledExtensionCount = static_cast<u32>(wantedExtensions.size()),
			.ppEnabledExtensionNames = wantedExtensions.data(),
			.pEnabledFeatures = nullptr,
		},
		vk::PhysicalDeviceFeatures2{},
		vk::PhysicalDeviceVulkan12Features{
			.descriptorIndexing = vk::True,
			.bufferDeviceAddress = vk::True,
		},
		vk::PhysicalDeviceVulkan13Features{
			.synchronization2 = vk::True,
			.dynamicRendering = vk::True,
		},
	};

	auto result = vk::Result{};
	std::tie(result, _logicalDevice) = _physicalDevice.createDevice(createInfoChain.get());

	VkAssert(result, "could not create logical device");

	auto const gfxQueueIdx = _queueFamilyIndices.gfx.value();
	auto const presentQueueIdx = _queueFamilyIndices.present.value();

	_gfxQueue = _logicalDevice.getQueue(gfxQueueIdx, 0u);
	_presentQueue = _logicalDevice.getQueue(presentQueueIdx, 0u);

	LogTrace("vk::Device created successfully");
	LogInfo("vk::Queue(graphics) selected: family index {}", gfxQueueIdx);
	LogInfo("vk::Queue(present) selected: family index {}", presentQueueIdx);
}

void VkContext::createSwapchain()
{
	auto const swapchainProps = getSwapchainProperties(_physicalDevice, _surface);

	auto const surfaceFormat = chooseSurfaceFormat(swapchainProps.formats);
	auto const swapchainPresentMode = chooseSwapchainPresentMode(swapchainProps.presentModes);
	auto const imageExtent = chooseImageExtent(swapchainProps.capabilities, _window);
	auto const numImages = [&] {
		auto const minCount = swapchainProps.capabilities.minImageCount;
		auto const maxCount = swapchainProps.capabilities.maxImageCount;

		if (maxCount == 0)
		{
			// maxImageCount == 0 means no limit
			return minCount + 1;
		}

		return std::min(minCount + 1, maxCount);
	}();

	auto const familyIndicesArr = std::array{ _queueFamilyIndices.gfx.value(), _queueFamilyIndices.present.value() };
	auto const hasMultipleQueues = _queueFamilyIndices.gfx != _queueFamilyIndices.present;

	auto const swapchainInfo = vk::SwapchainCreateInfoKHR{
		.surface = _surface,
		.minImageCount = numImages,
		.imageFormat = surfaceFormat.format,
		.imageColorSpace = surfaceFormat.colorSpace,
		.imageExtent = imageExtent,
		.imageArrayLayers = 1,
		.imageUsage = vk::ImageUsageFlagBits::eColorAttachment | vk::ImageUsageFlagBits::eTransferDst,
		.imageSharingMode = (hasMultipleQueues) ? vk::SharingMode::eConcurrent : vk::SharingMode::eExclusive,
		.queueFamilyIndexCount = (hasMultipleQueues) ? static_cast<u32>(familyIndicesArr.size()) : 0u,
		.pQueueFamilyIndices = (hasMultipleQueues) ? familyIndicesArr.data() : nullptr,
		.preTransform = swapchainProps.capabilities.currentTransform,
		.compositeAlpha = vk::CompositeAlphaFlagBitsKHR::eOpaque,
		.presentMode = swapchainPresentMode,
		.clipped = vk::True,
		.oldSwapchain = nullptr,
	};

	auto result = vk::Result{};
	std::tie(result, _swapchain) = _logicalDevice.createSwapchainKHR(swapchainInfo);
	VkAssert(result, "could not create swapchain");

	LogTrace("vk::SwapchainKHR created successfully");

	std::tie(result, _swapchainImages) = _logicalDevice.getSwapchainImagesKHR(_swapchain);
	VkAssert(result, "could not retrieve swapchain images");

	_imageFormat = surfaceFormat.format;
	_imageExtent = imageExtent;

	LogInfo("vk::Image: retrieved {} swapchain images", _swapchainImages.size());
	LogInfo("vk::Format selected: {}", std::to_underlying(_imageFormat));
	LogInfo("vk::Extent2D selected: {}x{}", _imageExtent.width, _imageExtent.height);
}

void VkContext::createImageViews()
{
	_imageViews.resize(_swapchainImages.size());

	vk::ImageViewCreateInfo createInfo{
			.viewType = vk::ImageViewType::e2D,
			.format = _imageFormat,
			.components = {
				.r = vk::ComponentSwizzle::eIdentity,
				.g = vk::ComponentSwizzle::eIdentity,
				.b = vk::ComponentSwizzle::eIdentity,
				.a = vk::ComponentSwizzle::eIdentity,
			},
			.subresourceRange = {
				.aspectMask = vk::ImageAspectFlagBits::eColor,
				.baseMipLevel = 0,
				.levelCount = 1,
				.baseArrayLayer = 0,
				.layerCount = 1,
			},
		};

	for (usize idx = 0u; idx < _swapchainImages.size(); ++idx)
	{
		createInfo.image = _swapchainImages[idx];

		auto result = vk::Result{};
		std::tie(result, _imageViews[idx]) = _logicalDevice.createImageView(createInfo);
		VkAssert(result, "could not create image view for swapchain image at idx {}", idx);

		LogTrace("vk::ImageView({}) created successfully", idx);
	}

	LogInfo("vk::ImageView (x{}) created successfully", _imageViews.size());
}

VkContext::VkContext(SDL_Window* window)
	: _window{ window }
{
	VULKAN_HPP_DEFAULT_DISPATCHER.init();

	createInstance();
	VULKAN_HPP_DEFAULT_DISPATCHER.init(_instance);

	createDbgMessenger();
	createSurface();
	selectPhysicalDevice();

	createLogicalDevice();
	VULKAN_HPP_DEFAULT_DISPATCHER.init(_logicalDevice);

	createSwapchain();
	createImageViews();
}

VkContext::~VkContext()
{
	for (auto imageView : _imageViews)
	{
		_logicalDevice.destroyImageView(imageView);
	}

	_logicalDevice.destroySwapchainKHR(_swapchain);
	_logicalDevice.destroy();

	SDL_Vulkan_DestroySurface(_instance, _surface, nullptr);

	if (sc::bcfg::enableValidationLayers())
	{
		_instance.destroyDebugUtilsMessengerEXT(_dbgMessenger);
	}

	_instance.destroy();
}

}
