// Simulatore Calcistico (SC): a football simulator
// Copyright 2024-2025 the SC contributors. See git history for details.
//
// This file is part of Simulatore Calcistico (SC).
//
// SC is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with SC.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include <any>
#include <initializer_list>
#include <optional>
#include <string_view>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include <sc/std/fundamental_types.hpp>

namespace cmdline
{

template <typename T>
auto tryParseCustom(std::string_view) -> std::optional<T>;

}

class CmdLineArg
{
	std::unordered_set<std::string_view> _forms;

public:
	explicit CmdLineArg(std::initializer_list<std::string_view>&& forms);
	[[nodiscard]] auto getForms() const -> std::unordered_set<std::string_view> const&;
	[[nodiscard]] auto hasForm(std::string_view form) const -> bool;
};

class CmdLine
{
	std::vector<std::string_view> _argv;
	std::unordered_map<std::string_view, std::any> _parsedForms;

public:
	CmdLine(usize argc, char const* const* argv);
	explicit CmdLine(std::initializer_list<std::string_view>&& argv);

	template <typename Func>
	void parseArgs(Func&& f);

	[[nodiscard]] auto hasFlag(std::string_view form) const -> bool;

	template <typename T>
		requires(!std::is_same_v<T, void>)
	[[nodiscard]] auto getValue(std::string_view form) const -> std::optional<T>;

	template <typename T>
		requires(!std::is_same_v<T, void>)
	[[nodiscard]] auto getValueOr(std::string_view form, T&& defVal) const -> T;
};

using ParseResult = std::tuple<bool, std::any, CmdLineArg>;

#define ParseArgNamed(FormVarName, ValVarName, Type, ...) \
	{ \
		static auto const _cmdline__arg__ = CmdLineArg{ __VA_ARGS__ }; \
		if (_cmdline__arg__.hasForm(FormVarName)) \
		{ \
			if constexpr (std::is_same_v<Type, void>) \
			{ \
				return ParseResult{ true, std::nullopt, _cmdline__arg__ }; \
			} \
			else \
			{ \
				auto const parsed = tryParse<Type>(ValVarName); \
				if (parsed) \
				{ \
					return ParseResult{ true, parsed, _cmdline__arg__ }; \
				} \
			} \
		} \
	}

#define ParseArg(Type, ...) ParseArgNamed(form, val, Type, __VA_ARGS__)
#define UnknownArg ParseResult{ false, std::nullopt, CmdLineArg{} };
