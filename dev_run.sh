#! /usr/bin/env bash
scriptDir=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &> /dev/null && pwd)

LD_LIBRARY_PATH="$scriptDir"/zig-out/lib VK_ADD_LAYER_PATH="$scriptDir"/zig-out/lib zig build -Dvalidation-layers run -- "$@"
