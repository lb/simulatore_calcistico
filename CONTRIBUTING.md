# Contributing to SC

## Ways to contribute
Todo

## Developer Certificate of Origin (DCO)

The DCO is a lightweight way of guaranteeing that contributors maintain  
their individual intellectual property rights by allowing every  
contribution to be properly attributed and licensed.

Every contribution must contain a `Signed-off-by` statement in the  
commit message to agree with the DCO.

These statements can be added automatically by git by providing the `-s`  
or `--signoff` flag to the `git commit` command, if your `user.name`  
and `user.email` are set in your git config.

The DCO is found below or at <https://developercertificate.org>.

```
Developer Certificate of Origin
Version 1.1

Copyright (C) 2004, 2006 The Linux Foundation and its contributors.

Everyone is permitted to copy and distribute verbatim copies of this
license document, but changing it is not allowed.


Developer's Certificate of Origin 1.1

By making a contribution to this project, I certify that:

(a) The contribution was created in whole or in part by me and I
    have the right to submit it under the open source license
    indicated in the file; or

(b) The contribution is based upon previous work that, to the best
    of my knowledge, is covered under an appropriate open source
    license and I have the right under that license to submit that
    work with modifications, whether created in whole or in part
    by me, under the same open source license (unless I am
    permitted to submit under a different license), as indicated
    in the file; or

(c) The contribution was provided directly to me by some other
    person who certified (a), (b) or (c) and I have not modified
    it.

(d) I understand and agree that this project and the contribution
    are public and that a record of the contribution (including all
    personal information I submit with it, including my sign-off) is
    maintained indefinitely and may be redistributed consistent with
    this project or the open source license(s) involved.
```

Each commit must include a DCO in the following form
```
Signed-off-by: Public Name <local-part@domain>
```

It is preferred but not required for the public name to be a real name.  
The email of the sign-off must match the email of the committer.
