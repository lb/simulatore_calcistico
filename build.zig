// Simulatore Calcistico (SC): a football simulator
// Copyright 2024-2025 the SC contributors. See git history for details.
//
// This file is part of Simulatore Calcistico (SC).
//
// SC is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with SC.  If not, see <https://www.gnu.org/licenses/>.

const std = @import("std");
const clangd_gen = @import("vendor/clangd_gen.zig");
const cmake = @import("vendor/cmake.zig");

pub fn build(b: *std.Build) void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

    const enable_profile = b.option(bool, "profile", "Enable profiling") orelse false;
    const enable_tests = b.option(bool, "test", "Enable tests") orelse false;
    const enable_validation_layers = b.option(bool, "validation-layers", "Enable vulkan validation layers") orelse false;
    const enable_asserts = b.option(bool, "asserts", "Enable asserts") orelse true;

    const sc = b.addExecutable(.{
        .name = "sc",
        .root_source_file = b.path("dummy.zig"),
        .target = target,
        .optimize = optimize,
    });

    addBuildConfigDefines(sc, .{
        .enable_validation_layers = enable_validation_layers,
        .enable_asserts = enable_asserts,
    });

    sc.addIncludePath(b.path("."));
    sc.addCSourceFiles(.{
        .files = sc_sources,
        .flags = compile_flags,
    });

    sc.linkLibCpp();

    addFlecs(b, sc, .ReleaseFast);
    addGlm(b, sc, .ReleaseFast);
    addSdl(b, sc, .ReleaseFast);
    addDoctest(b, sc, .ReleaseFast);
    addQuill(b, sc, .ReleaseFast);
    addVkHeaders(b, sc);
    addVkLoader(b, sc, .ReleaseFast);

    if (enable_validation_layers) {
        addVkValidationLayers(b, sc, .ReleaseFast);
    }

    if (enable_tests) {
        addNanobench(b, sc, .ReleaseFast);
    }

    if (enable_profile) {
        addTracy(b, sc, .ReleaseFast);
    }

    b.installArtifact(sc);

    {
        const run_cmd = b.addRunArtifact(sc);
        run_cmd.step.dependOn(b.getInstallStep());
        if (b.args) |args| {
            run_cmd.addArgs(args);
        }

        const run_step = b.step("run", "Run Simulatore Calcistico");
        run_step.dependOn(&run_cmd.step);
    }

    {
        const write_clangd = clangd_gen.addWriteClangd(b);
        const clangd_path = write_clangd.add("clangd", &.{sc}, clangd_conf);
        const install_clangd = b.addInstallFile(clangd_path, "clangd");

        install_clangd.step.dependOn(&write_clangd.step);
        b.getInstallStep().dependOn(&install_clangd.step);
    }
}

fn addDefines(c: *std.Build.Step.Compile, defines: anytype) void {
    inline for (defines) |define| {
        c.root_module.addCMacro(define[0], define[1]);
    }
}

fn addBuildConfigDefines(c: *std.Build.Step.Compile, build_config: anytype) void {
    for (buildConfigDefines(build_config)) |macro| {
        if (macro[0]) {
            c.root_module.addCMacro(macro[1], macro[2]);
        }
    }
}

fn addFlecs(b: *std.Build, sc: *std.Build.Step.Compile, optimize: std.builtin.OptimizeMode) void {
    const flecs = b.dependency("flecs", .{});
    sc.addIncludePath(flecs.path("include"));

    const cmake_build = cmake.addExec(b, &.{
        .name = "flecs",
        .source_dir = flecs.path("."),
        .build_type = cmake.BuildType.fromOptimize(optimize),
        .options = &.{
            .{ "FLECS_STATIC", false },
        },
    });

    b.getInstallStep().dependOn(&cmake_build.step);

    const lib_name = "libflecs.so";
    const artifact = cmake_build.artifact(lib_name);
    sc.addObjectFile(artifact);

    const install_artifact = b.addInstallLibFile(artifact, lib_name);
    b.getInstallStep().dependOn(&install_artifact.step);
}

fn addGlm(b: *std.Build, sc: *std.Build.Step.Compile, optimize: std.builtin.OptimizeMode) void {
    const glm = b.dependency("glm", .{});
    sc.addIncludePath(glm.path("."));

    const cmake_build = cmake.addExec(b, &.{
        .name = "glm",
        .source_dir = glm.path("."),
        .build_type = cmake.BuildType.fromOptimize(optimize),
    });

    const artifact = cmake_build.artifact("glm/libglm.a");
    sc.addObjectFile(artifact);
}

fn addSdl(b: *std.Build, sc: *std.Build.Step.Compile, optimize: std.builtin.OptimizeMode) void {
    const sdl = b.dependency("sdl", .{});
    sc.addIncludePath(sdl.path("include"));

    const cmake_build = cmake.addExec(b, &.{
        .name = "SDL3",
        .source_dir = sdl.path("."),
        .build_type = cmake.BuildType.fromOptimize(optimize),
        .options = &.{
            .{ "SDL_TEST_LIBRARY", false },
        },
    });

    b.getInstallStep().dependOn(&cmake_build.step);

    inline for (&.{ "libSDL3.so", "libSDL3.so.0", "libSDL3.so.0.2.4" }) |lib_name| {
        const artifact = cmake_build.artifact(lib_name);
        sc.addObjectFile(artifact);

        const install_artifact = b.addInstallLibFile(artifact, lib_name);
        b.getInstallStep().dependOn(&install_artifact.step);
    }
}

fn addDoctest(b: *std.Build, sc: *std.Build.Step.Compile, optimize: std.builtin.OptimizeMode) void {
    const doctest = b.dependency("doctest", .{});

    sc.addIncludePath(doctest.path("."));
    sc.addCSourceFiles(.{
        .files = &.{"sc/doctest.cpp"},
        .flags = compile_flags,
    });

    const cmake_build = cmake.addExec(b, &.{
        .name = "doctest",
        .source_dir = doctest.path("."),
        .build_type = cmake.BuildType.fromOptimize(optimize),
        .options = &.{
            .{ "DOCTEST_WITH_TESTS", false },
            .{ "DOCTEST_WITH_MAIN_IN_STATIC_LIB", false },
            .{ "DOCTEST_NO_INSTALL", true },
        },
    });

    b.getInstallStep().dependOn(&cmake_build.step);
    addDefines(sc, doctest_defines);
}

fn addQuill(b: *std.Build, sc: *std.Build.Step.Compile, optimize: std.builtin.OptimizeMode) void {
    const quill = b.dependency("quill", .{});

    sc.addIncludePath(quill.path("include"));
    sc.addCSourceFiles(.{
        .files = &.{"sc/quill.cpp"},
        .flags = compile_flags,
    });

    const cmake_build = cmake.addExec(b, &.{
        .name = "quill",
        .source_dir = quill.path("."),
        .build_type = cmake.BuildType.fromOptimize(optimize),
        .options = &.{
            .{ "QUILL_NO_EXCEPTIONS", true },
            .{ "QUILL_DISABLE_NON_PREFIXED_MACROS", true },
        },
    });

    b.getInstallStep().dependOn(&cmake_build.step);
    addDefines(sc, quill_defines);
}

fn addVkLoader(b: *std.Build, sc: *std.Build.Step.Compile, optimize: std.builtin.OptimizeMode) void {
    const vk_loader = b.dependency("vulkan-loader", .{});

    const cmake_build = cmake.addExec(b, &.{
        .name = "vulkan loader",
        .source_dir = vk_loader.path("."),
        .build_type = cmake.BuildType.fromOptimize(optimize),
        .options = &.{
            .{ "UPDATE_DEPS", true },
        },
    });

    b.getInstallStep().dependOn(&cmake_build.step);

    inline for (&.{ "libvulkan.so", "libvulkan.so.1", "libvulkan.so.1.4.304" }) |lib_name| {
        const artifact = cmake_build.artifact("loader/" ++ lib_name);
        sc.addObjectFile(artifact);

        const install_artifact = b.addInstallLibFile(artifact, lib_name);
        b.getInstallStep().dependOn(&install_artifact.step);
    }
}

fn addVkHeaders(b: *std.Build, c: *std.Build.Step.Compile) void {
    c.addIncludePath(b.dependency("vulkan-headers", .{}).path("include"));
    addDefines(c, vk_hpp_defines);
}

fn addVkValidationLayers(b: *std.Build, sc: *std.Build.Step.Compile, optimize: std.builtin.OptimizeMode) void {
    const vk_validation_layers = b.dependency("vulkan-validation-layers", .{});

    const cmake_build = cmake.addExec(b, &.{
        .name = "vk validation layers",
        .source_dir = vk_validation_layers.path("."),
        .build_type = cmake.BuildType.fromOptimize(optimize),
        .options = &.{
            .{ "UPDATE_DEPS", true },
        },
    });

    b.getInstallStep().dependOn(&cmake_build.step);

    const lib_name = "libVkLayer_khronos_validation.so";
    const artifact = cmake_build.artifact("layers/" ++ lib_name);
    sc.addObjectFile(artifact);

    const install_artifact = b.addInstallLibFile(artifact, lib_name);
    b.getInstallStep().dependOn(&install_artifact.step);

    const manifest_name = "VkLayer_khronos_validation.json";
    const manifest = cmake_build.artifact("layers/" ++ manifest_name);
    const install_manifest = b.addInstallLibFile(manifest, manifest_name);
    b.getInstallStep().dependOn(&install_manifest.step);
}

fn addNanobench(b: *std.Build, sc: *std.Build.Step.Compile, optimize: std.builtin.OptimizeMode) void {
    const nanobench = b.lazyDependency("nanobench", .{});
    if (nanobench == null) return;

    sc.addIncludePath(nanobench.?.path("src/include"));
    sc.addCSourceFiles(.{
        .files = &.{"sc/nanobench.cpp"},
        .flags = &.{"-std=c++23"},
    });

    const cmake_build = cmake.addExec(b, &.{
        .name = "nanobench",
        .source_dir = nanobench.?.path("."),
        .build_type = cmake.BuildType.fromOptimize(optimize),
    });

    b.getInstallStep().dependOn(&cmake_build.step);
}

fn addTracy(b: *std.Build, sc: *std.Build.Step.Compile, optimize: std.builtin.OptimizeMode) void {
    const tracy = b.lazyDependency("tracy", .{});
    if (tracy == null) return;

    sc.addIncludePath(tracy.?.path("public"));

    // client
    {
        const cmake_build = cmake.addExec(b, &.{
            .name = "tracy client",
            .source_dir = tracy.?.path("."),
            .build_type = cmake.BuildType.fromOptimize(optimize),
            .options = &.{
                .{ "TRACY_STATIC", false },
            },
        });

        b.getInstallStep().dependOn(&cmake_build.step);
        addDefines(sc, tracy_defines);

        inline for (&.{ "libTracyClient.so", "libTracyClient.so.0.11.1" }) |lib_name| {
            const artifact = cmake_build.artifact(lib_name);
            sc.addObjectFile(artifact);

            const install_artifact = b.addInstallLibFile(artifact, lib_name);
            b.getInstallStep().dependOn(&install_artifact.step);
        }
    }

    // server
    {
        const cmake_build = cmake.addExec(b, &.{
            .name = "tracy server",
            .source_dir = tracy.?.path("profiler"),
            .build_type = cmake.BuildType.fromOptimize(optimize),
        });

        b.getInstallStep().dependOn(&cmake_build.step);

        const profiler_name = "tracy-profiler";
        const artifact = cmake_build.artifact(profiler_name);
        const install_artifact = b.addInstallBinFile(artifact, profiler_name);
        b.getInstallStep().dependOn(&install_artifact.step);
    }
}

const sc_sources: []const []const u8 = &.{
    "sc/main.cpp",
    "sc/cmdline.cpp",
    "sc/std/log.cpp",
    "sc/rendering/vulkan/vk_context.cpp",
    "sc/rendering/vulkan/vk_renderer.cpp",
};

const compile_flags: []const []const u8 = &.{
    "-std=c++23",
    "-Wall",
    "-Wextra",
    "-g3",
    "-fno-omit-frame-pointer",
    "-fno-exceptions",
    "-fno-rtti",
    "-fno-strict-aliasing",
};

const BuildConfigMacro = struct { bool, []const u8, []const u8 };
fn buildConfigDefines(build_config: anytype) []const BuildConfigMacro {
    return &.{
        .{ build_config.enable_validation_layers, "SC_ENABLE_VULKAN_VALIDATION_LAYERS", "" },
        .{ build_config.enable_asserts, "SC_ENABLE_ASSERTS", "" },
    };
}

const vk_hpp_defines = .{
    .{ "VULKAN_HPP_NO_EXCEPTIONS", "" },
    .{ "VULKAN_HPP_NO_CONSTRUCTORS", "" },
    .{ "VULKAN_HPP_DISPATCH_LOADER_DYNAMIC", "1" },
};

const doctest_defines = .{
    .{ "DOCTEST_CONFIG_NO_EXCEPTIONS", "" },
    .{ "DOCTEST_CONFIG_SUPER_FAST_ASSERTS", "" },
};

const tracy_defines = .{
    .{ "TRACY_ENABLE", "" },
    .{ "TRACY_ON_DEMAND", "" },
    .{ "TRACY_NO_SAMPLING", "" },
};

const quill_defines = .{
    .{ "QUILL_NO_EXCEPTIONS", "" },
    .{ "QUILL_DISABLE_NON_PREFIXED_MACROS", "" },
};

const clangd_conf = clangd_gen.Clangd{
    .compile_flags = .{ .add = compile_flags },
    .diagnostics = .{
        .unused_includes = .strict,
        .clang_tidy = .{
            .add = &.{
                "bugprone*",
                "cppcoreguidelines*",
                "misc*",
                "modernize*",
                "performance*",
                "portability*",
                "readability*",
            },
            .remove = &.{
                "bugprone-lambda-function-name",
                "cppcoreguidelines-avoid-magic-numbers",
                "cppcoreguidelines-owning-memory",
                "cppcoreguidelines-pro-bounds-constant-array-index",
                "cppcoreguidelines-pro-bounds-pointer-arithmetic",
                "misc-non-private-member-variables-in-classes",
                "readability-function-cognitive-complexity",
                "readability-identifier-length",
                "readability-magic-numbers",
                "readability-uppercase-literal-suffix",
            },
            .check_options = &.{
                .{ .key = "cppcoreguidelines-avoid-do-while.IgnoreMacros", .value = "true" },
            },
        },
    },
};
