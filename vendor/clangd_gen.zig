// Simulatore Calcistico (SC): a football simulator
// Copyright 2024 the SC contributors. See git history for details.
//
// This file is part of Simulatore Calcistico (SC).
//
// SC is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with SC.  If not, see <https://www.gnu.org/licenses/>.

const std = @import("std");

pub const Clangd = struct {
    compile_flags: ?CompileFlags = null,
    index: ?Index = null,
    style: ?Style = null,
    diagnostics: ?Diagnostics = null,
    completion: ?Completion = null,
    inlay_hints: ?InlayHints = null,
    hover: ?Hover = null,
    semantic_tokens: ?SemanticTokens = null,

    pub fn dupe(conf: Clangd, b: *std.Build) Clangd {
        return .{
            .compile_flags = if (conf.compile_flags) |f| f.dupe(b) else null,
            .index = if (conf.index) |i| i.dupe(b) else null,
            .style = if (conf.style) |s| s.dupe(b) else null,
            .diagnostics = if (conf.diagnostics) |d| d.dupe(b) else null,
            .completion = conf.completion,
            .inlay_hints = conf.inlay_hints,
            .hover = conf.hover,
            .semantic_tokens = if (conf.semantic_tokens) |s| s.dupe(b) else null,
        };
    }

    pub fn write(conf: *const Clangd, bw: anytype) !void {
        if (conf.compile_flags) |f| try f.write(bw, 0);
        if (conf.index) |i| try i.write(bw, 0);
        if (conf.style) |s| try s.write(bw, 0);
        if (conf.diagnostics) |d| try d.write(bw, 0);
        if (conf.completion) |c| try c.write(bw, 0);
        if (conf.inlay_hints) |i| try i.write(bw, 0);
        if (conf.hover) |h| try h.write(bw, 0);
        if (conf.semantic_tokens) |s| try s.write(bw, 0);
    }

    pub fn allocBytes(self: Clangd, allocator: std.mem.Allocator) ![]const u8 {
        var bytes = try std.ArrayList(u8).initCapacity(allocator, 4096);
        defer bytes.deinit();

        if (self.compile_flags) |flags| {
            for (flags.add) |e| {
                try bytes.appendSlice(e);
            }

            for (flags.remove) |e| {
                try bytes.appendSlice(e);
            }

            if (flags.compilation_database) |db| {
                switch (db) {
                    .none => try bytes.append(@as(u8, 0)),
                    .ancestors => try bytes.append(@as(u8, 1)),
                    .dir => |dir| try bytes.appendSlice(dir),
                }
            }

            try bytes.appendSlice(flags.compiler);
        }

        if (self.index) |index| {
            if (index.background) |bg| {
                try bytes.append(@as(u8, @intFromEnum(bg)));
            }

            if (index.external) |ext| {
                switch (ext.kind) {
                    .file => |f| try bytes.appendSlice(f),
                    .server => |s| try bytes.appendSlice(s),
                }

                try bytes.appendSlice(ext.mount_point);
            }

            if (index.standard_library) |stdlib| {
                try bytes.append(@as(u8, @intFromBool(stdlib)));
            }
        }

        if (self.style) |style| {
            for (style.fully_qualified_namespaces) |ns| {
                try bytes.appendSlice(ns);
            }
        }

        if (self.diagnostics) |diag| {
            for (diag.suppress) |suppress| {
                try bytes.appendSlice(suppress);
            }

            if (diag.clang_tidy) |tidy| {
                for (tidy.add) |add| {
                    try bytes.appendSlice(add);
                }

                for (tidy.remove) |remove| {
                    try bytes.appendSlice(remove);
                }

                for (tidy.check_options) |opt| {
                    try bytes.appendSlice(opt.key);
                    try bytes.appendSlice(opt.value);
                }

                if (tidy.fast_check_filter) |fcf| {
                    try bytes.append(@as(u8, @intFromEnum(fcf)));
                }
            }

            if (diag.unused_includes) |ui| {
                try bytes.append(@as(u8, @intFromEnum(ui)));
            }

            if (diag.includes) |includes| {
                for (includes.ignore_header) |ih| {
                    try bytes.appendSlice(ih);
                }

                if (includes.analyze_angled_includes) |anal| {
                    try bytes.append(@as(u8, @intFromBool(anal)));
                }
            }

            if (diag.missing_includes) |mi| {
                try bytes.append(@as(u8, @intFromEnum(mi)));
            }
        }

        if (self.completion) |completion| {
            if (completion.all_scopes) |all_scopes| {
                try bytes.append(@as(u8, @intFromBool(all_scopes)));
            }
        }

        if (self.inlay_hints) |hints| {
            if (hints.enabled) |flag| {
                try bytes.append(@as(u8, @intFromBool(flag)));
            }

            if (hints.parameter_names) |flag| {
                try bytes.append(@as(u8, @intFromBool(flag)));
            }

            if (hints.deduced_types) |flag| {
                try bytes.append(@as(u8, @intFromBool(flag)));
            }

            if (hints.designators) |flag| {
                try bytes.append(@as(u8, @intFromBool(flag)));
            }

            if (hints.block_end) |flag| {
                try bytes.append(@as(u8, @intFromBool(flag)));
            }

            if (hints.type_name_limit) |limit| {
                var buf: [2]u8 = undefined;
                std.mem.writeInt(u16, &buf, limit, .little);
                try bytes.appendSlice(&buf);
            }
        }

        if (self.hover) |hover| {
            if (hover.show_aka) |aka| {
                try bytes.append(@as(u8, @intFromBool(aka)));
            }
        }

        if (self.semantic_tokens) |tokens| {
            for (tokens.disabled_kinds) |kind| {
                try bytes.append(@as(u8, @intFromEnum(kind)));
            }

            for (tokens.disabled_modifiers) |mod| {
                try bytes.append(@as(u8, @intFromEnum(mod)));
            }
        }

        return try bytes.toOwnedSlice();
    }

    pub const CompileFlags = struct {
        add: []const []const u8 = &.{},
        remove: []const []const u8 = &.{},
        compilation_database: ?CompilationDatabase = null,
        compiler: []const u8 = "",

        pub fn dupe(flags: CompileFlags, b: *std.Build) CompileFlags {
            return .{
                .add = b.dupeStrings(flags.add),
                .remove = b.dupeStrings(flags.remove),
                .compilation_database = if (flags.compilation_database) |c| c.dupe(b) else null,
                .compiler = b.dupe(flags.compiler),
            };
        }

        pub fn write(flags: *const CompileFlags, bw: anytype, comptime indent: u8) !void {
            try writeHeading("CompileFlags", bw, indent);
            try writeArr([]const u8, "Add", flags.add, bw, indent + 1);
            try writeArr([]const u8, "Remove", flags.remove, bw, indent + 1);
            if (flags.compilation_database) |c| try c.write(bw, indent + 1);
            try writeVal("Compiler", flags.compiler, bw, indent + 1);
        }

        pub const CompilationDatabase = union(enum) {
            none,
            ancestors,
            dir: []const u8,

            pub fn dupe(db: CompilationDatabase, b: *std.Build) CompilationDatabase {
                switch (db) {
                    .none => return .{.none},
                    .ancestors => return .{.ancestors},
                    .dir => |dir| return .{ .dir = b.dupe(dir) },
                }
            }

            pub fn write(db: CompilationDatabase, bw: anytype, comptime indent: u8) !void {
                const k = "CompilationDatabase";
                switch (db) {
                    .ancestors => try writeVal(k, "Ancestors", bw, indent),
                    .none => try writeVal(k, "None", bw, indent),
                    .dir => |dir| try writeVal(k, dir, bw, indent),
                }
            }
        };
    };

    pub const Index = struct {
        background: ?Background = null,
        external: ?External = null,
        standard_library: ?bool = null,

        pub fn dupe(index: Index, b: *std.Build) Index {
            return .{
                .background = index.background,
                .external = if (index.external) |e| e.dupe(b) else null,
                .standard_library = index.standard_library,
            };
        }

        pub fn write(index: *const Index, bw: anytype, comptime indent: u8) !void {
            try writeHeading("Index", bw, indent);
            if (index.background) |b| try b.write(bw, indent + 1);
            if (index.external) |e| try e.write(bw, indent + 1);
            try writeBool("StandardLibrary", index.standard_library, bw, indent + 1);
        }

        pub const Background = enum {
            build,
            skip,

            pub fn write(bg: Background, bw: anytype, comptime indent: u8) !void {
                const k = "Background";
                switch (bg) {
                    .build => try writeVal(k, "Build", bw, indent),
                    .skip => try writeVal(k, "Skip", bw, indent),
                }
            }
        };

        pub const External = struct {
            kind: Kind,
            mount_point: []const u8 = "",

            pub fn dupe(external: External, b: *std.Build) External {
                return .{
                    .kind = external.kind.dupe(b),
                    .mount_point = b.dupe(external.mount_point),
                };
            }

            pub fn write(external: *const External, bw: anytype, comptime indent: u8) !void {
                try writeHeading("External", bw, indent);
                try external.kind.write(bw, indent + 1);
                try writeVal("MountPoint", external.mount_point, bw, indent + 1);
            }

            pub const Kind = union(enum) {
                file: []const u8,
                server: []const u8,

                pub fn dupe(kind: Kind, b: *std.Build) Kind {
                    switch (kind) {
                        .file => |file| return .{ .file = b.dupe(file) },
                        .server => |server| return .{ .server = b.dupe(server) },
                    }
                }

                pub fn write(kind: Kind, bw: anytype, comptime indent: u8) !void {
                    switch (kind) {
                        .file => |file| try writeVal("File", file, bw, indent),
                        .server => |server| try writeVal("Server", server, bw, indent),
                    }
                }
            };
        };
    };

    pub const Style = struct {
        fully_qualified_namespaces: []const []const u8 = &.{},

        pub fn dupe(style: Style, b: *std.Build) Style {
            return .{
                .fully_qualified_namespaces = b.dupeStrings(style.fully_qualified_namespaces),
            };
        }

        pub fn write(style: Style, bw: anytype, comptime indent: u8) !void {
            try writeHeading("Style", bw, indent);
            try writeArr([]const u8, "FullyQualifiedNamespaces", style.fully_qualified_namespaces, bw, indent + 1);
        }
    };

    pub const Diagnostics = struct {
        suppress: []const []const u8 = &.{},
        clang_tidy: ?ClangTidy = null,
        unused_includes: ?UnusedIncludes = null,
        includes: ?Includes = null,
        missing_includes: ?MissingIncludes = null,

        pub fn dupe(diagnostics: Diagnostics, b: *std.Build) Diagnostics {
            return .{
                .suppress = b.dupeStrings(diagnostics.suppress),
                .clang_tidy = if (diagnostics.clang_tidy) |c| c.dupe(b) else null,
                .unused_includes = diagnostics.unused_includes,
                .includes = if (diagnostics.includes) |i| i.dupe(b) else null,
                .missing_includes = diagnostics.missing_includes,
            };
        }

        pub fn write(diagnostics: Diagnostics, bw: anytype, comptime indent: u8) !void {
            try writeHeading("Diagnostics", bw, indent);
            try writeArr([]const u8, "Suppress", diagnostics.suppress, bw, indent + 1);
            if (diagnostics.clang_tidy) |c| try c.write(bw, indent + 1);
            if (diagnostics.unused_includes) |u| try u.write(bw, indent + 1);
            if (diagnostics.includes) |i| try i.write(bw, indent + 1);
            if (diagnostics.missing_includes) |m| try m.write(bw, indent + 1);
        }

        pub const ClangTidy = struct {
            add: []const []const u8 = &.{},
            remove: []const []const u8 = &.{},
            check_options: []const CheckOption = &.{},
            fast_check_filter: ?FastCheckFilter = null,

            pub fn dupe(clang_tidy: ClangTidy, b: *std.Build) ClangTidy {
                return .{
                    .add = b.dupeStrings(clang_tidy.add),
                    .remove = b.dupeStrings(clang_tidy.remove),
                    .check_options = dupeArr(CheckOption, clang_tidy.check_options, b),
                    .fast_check_filter = clang_tidy.fast_check_filter,
                };
            }

            pub fn write(clang_tidy: ClangTidy, bw: anytype, comptime indent: u8) !void {
                try writeHeading("ClangTidy", bw, indent);
                try writeArr([]const u8, "Add", clang_tidy.add, bw, indent + 1);
                try writeArr([]const u8, "Remove", clang_tidy.remove, bw, indent + 1);
                try writeMany(CheckOption, "CheckOptions", clang_tidy.check_options, bw, indent + 1);
                if (clang_tidy.fast_check_filter) |f| try f.write(bw, indent + 1);
            }

            pub const CheckOption = struct {
                key: []const u8,
                value: []const u8,

                pub fn dupe(option: CheckOption, b: *std.Build) CheckOption {
                    return .{
                        .key = b.dupe(option.key),
                        .value = b.dupe(option.value),
                    };
                }

                pub fn write(option: CheckOption, bw: anytype, comptime indent: u8) !void {
                    try writeVal(option.key, option.value, bw, indent + 1);
                }
            };

            pub const FastCheckFilter = enum {
                strict,
                loose,
                none,

                pub fn write(filter: FastCheckFilter, bw: anytype, comptime indent: u8) !void {
                    const k = "FastCheckFilter";
                    switch (filter) {
                        .strict => try writeVal(k, "Strict", bw, indent),
                        .loose => try writeVal(k, "Loose", bw, indent),
                        .none => try writeVal(k, "None", bw, indent),
                    }
                }
            };
        };

        pub const UnusedIncludes = enum {
            strict,
            none,

            pub fn write(unused_includes: UnusedIncludes, bw: anytype, comptime indent: u8) !void {
                const k = "UnusedIncludes";
                switch (unused_includes) {
                    .strict => try writeVal(k, "Strict", bw, indent),
                    .none => try writeVal(k, "None", bw, indent),
                }
            }
        };

        pub const Includes = struct {
            ignore_header: []const []const u8 = &.{},
            analyze_angled_includes: ?bool = null,

            pub fn dupe(includes: Includes, b: *std.Build) Includes {
                return .{
                    .ignore_header = b.dupeStrings(includes.ignore_header),
                    .analyze_angled_includes = includes.analyze_angled_includes,
                };
            }

            pub fn write(includes: Includes, bw: anytype, comptime indent: u8) !void {
                try writeHeading("Includes", bw, indent);
                try writeArr([]const u8, "IgnoreHeader", includes.ignore_header, bw, indent + 1);
                try writeBool("AnalyzeAngledIncludes", includes.analyze_angled_includes, bw, indent + 1);
            }
        };

        pub const MissingIncludes = enum {
            strict,
            none,

            pub fn write(missing_includes: MissingIncludes, bw: anytype, comptime indent: u8) !void {
                const k = "MissingIncludes";
                switch (missing_includes) {
                    .strict => try writeVal(k, "Strict", bw, indent),
                    .none => try writeVal(k, "None", bw, indent),
                }
            }
        };
    };

    pub const Completion = struct {
        all_scopes: ?bool = null,

        pub fn write(completion: Completion, bw: anytype, comptime indent: u8) !void {
            try writeHeading("Completion", bw, indent);
            try writeBool("AllScopes", completion.all_scopes, bw, indent + 1);
        }
    };

    pub const InlayHints = struct {
        enabled: ?bool = null,
        parameter_names: ?bool = null,
        deduced_types: ?bool = null,
        designators: ?bool = null,
        block_end: ?bool = null,
        type_name_limit: ?u16 = null,

        pub fn write(hints: InlayHints, bw: anytype, comptime indent: u8) !void {
            try writeHeading("InlayHints", bw, indent);
            try writeBool("Enabled", hints.enabled, bw, indent + 1);
            try writeBool("ParameterNames", hints.parameter_names, bw, indent + 1);
            try writeBool("DeducedTypes", hints.deduced_types, bw, indent + 1);
            try writeBool("Designators", hints.designators, bw, indent + 1);
            try writeBool("BlockEnd", hints.block_end, bw, indent + 1);
            try writeInt("TypeNameLimit", hints.type_name_limit, bw, indent + 1);
        }
    };

    pub const Hover = struct {
        show_aka: ?bool = null,

        pub fn write(hover: Hover, bw: anytype, comptime indent: u8) !void {
            try writeHeading("Hover", bw, indent);
            try writeBool("ShowAKA", hover.show_aka, bw, indent + 1);
        }
    };

    pub const SemanticTokens = struct {
        disabled_kinds: []const DisabledKinds = &.{},
        disabled_modifiers: []const DisabledModifiers = &.{},

        pub fn dupe(tokens: SemanticTokens, b: *std.Build) SemanticTokens {
            return .{
                .disabled_kinds = dupeArr(DisabledKinds, tokens.disabled_kinds, b),
                .disabled_modifiers = dupeArr(DisabledModifiers, tokens.disabled_modifiers, b),
            };
        }

        pub fn write(tokens: SemanticTokens, bw: anytype, comptime indent: u8) !void {
            try writeHeading("SemanticTokens", bw, indent);
            try writeArr(DisabledKinds, "DisabledKinds", tokens.disabled_kinds, bw, indent + 1);
            try writeArr(DisabledModifiers, "DisabledModifiers", tokens.disabled_modifiers, bw, indent + 1);
        }

        pub const DisabledKinds = enum {
            variable,
            local_variable,
            static_field,
            parameter,
            static_method,
            function,
            method,
            field,
            class,
            interface,
            @"enum",
            enum_constant,
            type,
            typedef,
            primitive,
            template_parameter,
            namespace,
            macro,
            modifier,
            operator,
            bracket,
            label,
            inactive_code,

            // clangd extension
            unknown,
            concept,

            pub fn toStr(e: DisabledKinds) []const u8 {
                return switch (e) {
                    .variable => "Variable",
                    .local_variable => "LocalVariable",
                    .static_field => "StaticField",
                    .parameter => "Parameter",
                    .static_method => "StaticMethod",
                    .function => "Function",
                    .method => "Method",
                    .field => "Field",
                    .class => "Class",
                    .interface => "Interface",
                    .@"enum" => "Enum",
                    .enum_constant => "EnumConstant",
                    .type => "Type",
                    .typedef => "Typedef",
                    .primitive => "Primitive",
                    .template_parameter => "TemplateParameter",
                    .namespace => "Namespace",
                    .macro => "Macro",
                    .modifier => "Modifier",
                    .operator => "Operator",
                    .bracket => "Bracket",
                    .label => "Label",
                    .inactive_code => "InactiveCode",
                    .unknown => "Unknown",
                    .concept => "Concept",
                };
            }
        };

        pub const DisabledModifiers = enum {
            declaration,
            definition,
            deprecated,
            readonly,
            static,
            abstract,
            default_library,

            // clangd extension
            deduced,
            virtual,
            dependent_name,
            used_as_mutable_reference,
            used_as_mutable_pointer,
            constructor_or_destructor,
            user_defined,
            function_scope,
            class_scope,
            file_scope,
            global_scope,

            pub fn toStr(e: DisabledModifiers) []const u8 {
                return switch (e) {
                    .declaration => "Declaration",
                    .definition => "Definition",
                    .deprecated => "Deprecated",
                    .readonly => "Readonly",
                    .static => "Static",
                    .abstract => "Abstract",
                    .default_library => "DefaultLibrary",
                    .deduced => "Deduced",
                    .virtual => "Virtual",
                    .dependent_name => "DependentName",
                    .used_as_mutable_reference => "UsedAsMutableReference",
                    .used_as_mutable_pointer => "UsedAsMutablePointer",
                    .constructor_or_destructor => "ConstructorOrDestructor",
                    .user_defined => "UserDefined",
                    .function_scope => "FunctionScope",
                    .class_scope => "ClassScope",
                    .file_scope => "FileScope",
                    .global_scope => "GlobalScope",
                };
            }
        };
    };
};

fn dupe(comptime T: type, val: T, b: *std.Build) T {
    const duped = b.allocator.alloc(T, 1) catch @panic("OOM");
    duped[0] = val;
    return duped[0];
}

fn dupeArr(comptime T: type, arr: []const T, b: *std.Build) []T {
    const duped = b.allocator.alloc([]T, arr.len) catch @panic("OOM");
    for (duped, arr) |*dest, source| dest.* = dupe(T, source, b);
    return duped;
}

pub const File = struct {
    generated_file: std.Build.GeneratedFile,
    sub_path: []const u8,
    targets: std.ArrayListUnmanaged(*std.Build.Step.Compile),
    conf: Clangd,

    pub fn getPath(file: *File) std.Build.LazyPath {
        return .{ .generated = .{ .file = &file.generated_file } };
    }
};

pub const WriteClangd = struct {
    step: std.Build.Step,
    files: std.ArrayListUnmanaged(*File),
    generated_directory: std.Build.GeneratedFile,

    pub fn create(owner: *std.Build) *WriteClangd {
        const write_clangd = owner.allocator.create(WriteClangd) catch @panic("OOM");

        write_clangd.* = .{
            .step = std.Build.Step.init(.{
                .id = .custom,
                .name = "WriteClangd",
                .owner = owner,
                .makeFn = makeClangd,
            }),
            .files = .{},
            .generated_directory = .{ .step = &write_clangd.step },
        };

        return write_clangd;
    }

    pub fn add(write_clangd: *WriteClangd, sub_path: []const u8, targets: []const *std.Build.Step.Compile, conf: Clangd) std.Build.LazyPath {
        const b = write_clangd.step.owner;
        const gpa = b.allocator;
        const file = gpa.create(File) catch @panic("OOM");
        file.* = .{
            .generated_file = .{ .step = &write_clangd.step },
            .sub_path = b.dupePath(sub_path),
            .targets = .{},
            .conf = conf,
        };
        file.targets.appendSlice(gpa, targets) catch @panic("OOM");

        write_clangd.files.append(gpa, file) catch @panic("OOM");
        write_clangd.maybeUpdateName();

        for (targets) |target| {
            write_clangd.step.dependOn(&target.step);
        }

        return file.getPath();
    }

    fn maybeUpdateName(write_clangd: *WriteClangd) void {
        if (write_clangd.files.items.len != 1) return;
        if (!std.mem.eql(u8, write_clangd.step.name, "WriteClangd")) return;

        const owner = write_clangd.step.owner;
        const file_name = write_clangd.files.items[0].sub_path;
        write_clangd.step.name = owner.fmt("WriteClangd {s}", .{file_name});
    }
};

pub fn addWriteClangd(b: *std.Build) *WriteClangd {
    return WriteClangd.create(b);
}

fn makeClangd(step: *std.Build.Step, node: std.Progress.Node) !void {
    _ = node;
    const b = step.owner;
    const write_clangd: *WriteClangd = @fieldParentPtr("step", step);

    var manifest = b.graph.cache.obtain();
    defer manifest.deinit();
    manifest.hash.add(@as(u32, 0x4f231a27));

    for (write_clangd.files.items) |file| {
        // need flags to participate in the hash too
        try populateFlags(b, &file.conf, &file.targets);

        manifest.hash.addBytes(file.sub_path);
        manifest.hash.addBytes(try file.conf.allocBytes(b.allocator));
        for (file.targets.items) |target| {
            manifest.hash.addBytes(target.step.name);
        }
    }

    if (try step.cacheHit(&manifest)) {
        const digest = manifest.final();
        for (write_clangd.files.items) |file| {
            file.generated_file.path = try b.cache_root.join(b.allocator, &.{
                "o", &digest, file.sub_path,
            });
        }

        write_clangd.generated_directory.path = try b.cache_root.join(b.allocator, &.{ "o", &digest });
        return;
    }

    const digest = manifest.final();
    const cache_path = "o" ++ std.fs.path.sep_str ++ digest;

    write_clangd.generated_directory.path = try b.cache_root.join(b.allocator, &.{ "o", &digest });

    var cache_dir = b.cache_root.handle.makeOpenPath(cache_path, .{}) catch |err| {
        return step.fail("unable to make path '{}{s}': {s}", .{
            b.cache_root, cache_path, @errorName(err),
        });
    };
    defer cache_dir.close();

    for (write_clangd.files.items) |file| {
        if (std.fs.path.dirname(file.sub_path)) |dirname| {
            cache_dir.makePath(dirname) catch |err| {
                return step.fail("unable to make path '{}{s}{c}{s}': {s}", .{
                    b.cache_root, cache_path, std.fs.path.sep, dirname, @errorName(err),
                });
            };
        }

        var contents = std.ArrayList(u8).init(b.allocator);
        defer contents.deinit();

        var buf_writer = std.io.bufferedWriter(contents.writer());

        writeFile(file, &buf_writer) catch @panic("OOM");
        buf_writer.flush() catch @panic("OOM");

        const yml = contents.toOwnedSlice() catch @panic("OOM");
        cache_dir.writeFile(.{ .sub_path = file.sub_path, .data = yml }) catch |err| {
            return step.fail("unable to write file '{}{s}{c}{s}': {s}", .{
                b.cache_root, cache_path, std.fs.path.sep, file.sub_path, @errorName(err),
            });
        };

        file.generated_file.path = try b.cache_root.join(b.allocator, &.{
            cache_path, file.sub_path,
        });
    }

    try step.writeManifest(&manifest);
}

fn populateFlags(b: *std.Build, conf: *Clangd, targets: *std.ArrayListUnmanaged(*std.Build.Step.Compile)) !void {
    var full_flags = try std.ArrayListUnmanaged([]const u8).initCapacity(b.allocator, 4096);
    defer full_flags.deinit(b.allocator);

    if (conf.compile_flags) |conf_flags| {
        try full_flags.appendSlice(b.allocator, conf_flags.add);
    }

    for (targets.items) |target| {
        var include_dirs = try std.ArrayListUnmanaged([]const u8).initCapacity(b.allocator, 4096);
        defer include_dirs.deinit(b.allocator);

        var c_macros = try std.ArrayListUnmanaged([]const u8).initCapacity(b.allocator, 4096);
        defer c_macros.deinit(b.allocator);

        try collectIncludeDirs(b, target, &include_dirs);
        for (include_dirs.items) |include_dir| {
            try full_flags.append(b.allocator, b.fmt("-I{s}", .{include_dir}));
        }

        try collectCMacros(b, target, &c_macros);
        for (c_macros.items) |c_macro| {
            try full_flags.append(b.allocator, c_macro);
        }
    }

    if (conf.compile_flags == null) {
        conf.compile_flags = .{};
    }

    conf.compile_flags.?.add = try full_flags.toOwnedSlice(b.allocator);
}

fn collectIncludeDirs(b: *std.Build, target: *std.Build.Step.Compile, out_dirs: *std.ArrayListUnmanaged([]const u8)) !void {
    for (target.root_module.include_dirs.items) |include_dir| {
        switch (include_dir) {
            .path => |path| try out_dirs.append(b.allocator, path.getPath(b)),
            .path_system => |path| try out_dirs.append(b.allocator, path.getPath(b)),
            .path_after => |path| try out_dirs.append(b.allocator, path.getPath(b)),
            .framework_path => |path| try out_dirs.append(b.allocator, path.getPath(b)),
            .framework_path_system => |path| try out_dirs.append(b.allocator, path.getPath(b)),
            .config_header_step => |config_header| try out_dirs.append(b.allocator, config_header.getOutput().dirname().getPath(b)),
            .other_step => |other_target| {
                for (other_target.installed_headers.items) |header| {
                    try out_dirs.append(b.allocator, header.getSource().getPath(b));
                }

                try collectIncludeDirs(b, other_target, out_dirs);
            },
        }
    }
}

fn collectCMacros(b: *std.Build, target: *std.Build.Step.Compile, out_macros: *std.ArrayListUnmanaged([]const u8)) !void {
    for (target.root_module.c_macros.items) |c_macro| {
        try out_macros.append(b.allocator, c_macro);
    }
}

inline fn indented(comptime str: []const u8, comptime indent: u8) []const u8 {
    const indent_str: []const u8 = "  ";
    return (indent_str ** indent) ++ str;
}

fn writeFile(f: *File, bw: anytype) !void {
    try f.conf.write(bw);
}

fn writeHeading(k: []const u8, bw: anytype, comptime indent: u8) !void {
    const fmt = indented("{s}:\n", indent);
    try bw.writer().print(fmt, .{k});
}

fn writeInt(k: []const u8, val: anytype, bw: anytype, comptime indent: u8) !void {
    if (val == null) return;

    const fmt = indented("{s}: {d}\n", indent);
    try bw.writer().print(fmt, .{ k, val.? });
}

fn writeBool(k: []const u8, val: ?bool, bw: anytype, comptime indent: u8) !void {
    if (val == null) return;
    try writeVal(k, if (val.?) "Yes" else "No", bw, indent);
}

fn writeVal(k: []const u8, val: []const u8, bw: anytype, comptime indent: u8) !void {
    if (val.len == 0) return;

    const fmt = indented("{s}: {s}\n", indent);
    try bw.writer().print(fmt, .{ k, val });
}

fn writeArr(comptime T: type, k: []const u8, arr: []const T, bw: anytype, comptime indent: u8) !void {
    if (arr.len == 0) return;

    const start_fmt = indented("{s}: [\n", indent);
    const entry_fmt = indented("{s},\n", indent + 1);
    const end_fmt = indented("]\n", indent);

    try bw.writer().print(start_fmt, .{k});
    for (arr) |v| {
        if (T == []const u8) {
            try bw.writer().print(entry_fmt, .{v});
        } else {
            try bw.writer().print(entry_fmt, .{v.toStr()});
        }
    }
    try bw.writer().print(end_fmt, .{});
}

fn writeMany(comptime T: type, k: []const u8, arr: []const T, bw: anytype, comptime indent: u8) !void {
    try writeHeading(k, bw, indent);
    for (arr) |elem| {
        try elem.write(bw, indent + 1);
    }
}
