// Simulatore Calcistico (SC): a football simulator
// Copyright 2025 the SC contributors. See git history for details.
//
// This file is part of Simulatore Calcistico (SC).
//
// SC is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with SC.  If not, see <https://www.gnu.org/licenses/>.

const std = @import("std");

pub const Generator = enum {
    UnixMakefiles,
    Ninja,
    NinjaMultiConfig,
    VisualStudio14,
    VisualStudio15,
    VisualStudio16,
    VisualStudio17,

    pub fn toStr(self: Generator) []const u8 {
        switch (self) {
            .UnixMakefiles => return "Unix Makefiles",
            .Ninja => return "Ninja",
            .NinjaMultiConfig => return "Ninja Multi-Config",
            .VisualStudio14 => return "Visual Studio 14 2015",
            .VisualStudio15 => return "Visual Studio 15 2017",
            .VisualStudio16 => return "Visual Studio 16 2019",
            .VisualStudio17 => return "Visual Studio 17 2022",
        }
    }
};

pub const BuildType = enum {
    Debug,
    Release,
    RelWithDebInfo,
    MinSizeRel,

    pub fn fromOptimize(o: std.builtin.OptimizeMode) BuildType {
        switch (o) {
            .Debug => return .Debug,
            .ReleaseSafe => return .RelWithDebInfo,
            .ReleaseFast => return .RelWithDebInfo,
            .ReleaseSmall => return .MinSizeRel,
        }
    }
};

pub const Option = struct { []const u8, bool };

pub const CreateOpts = struct {
    name: []const u8,
    source_dir: std.Build.LazyPath,
    generator: Generator = .Ninja,
    build_type: BuildType = .Debug,
    parallel: bool = true,
    options: []const Option = &.{},
};

pub const Exec = struct {
    step: std.Build.Step,
    build_dir: std.Build.GeneratedFile,
    opts: CreateOpts,

    pub fn create(owner: *std.Build, opts: *const CreateOpts) *Exec {
        const cmake_build = owner.allocator.create(Exec) catch @panic("OOM");

        cmake_build.* = .{
            .step = std.Build.Step.init(.{
                .id = .custom,
                .name = owner.fmt("cmake {s}", .{opts.name}),
                .owner = owner,
                .makeFn = makeCmakeBuild,
            }),
            .build_dir = .{ .step = &cmake_build.step },
            .opts = opts.*,
        };

        return cmake_build;
    }

    pub fn buildRoot(self: *const Exec) std.Build.LazyPath {
        return .{ .generated = .{ .file = &self.build_dir } };
    }

    pub fn artifact(self: *Exec, name: []const u8) std.Build.LazyPath {
        return self.buildRoot().path(self.step.owner, name);
    }
};

pub fn addExec(b: *std.Build, opts: *const CreateOpts) *Exec {
    return Exec.create(b, opts);
}

fn makeCmakeBuild(step: *std.Build.Step, node: std.Progress.Node) !void {
    const b = step.owner;
    const cmake_build: *Exec = @fieldParentPtr("step", step);

    var manifest = b.graph.cache.obtain();
    defer manifest.deinit();

    manifest.hash.add(@as(u32, 0x849d1749));
    manifest.hash.addBytes(cmake_build.opts.name);
    manifest.hash.addBytes(cmake_build.opts.source_dir.getPath(b));

    _ = try step.cacheHit(&manifest); // ignore cache, always rerun configure and build
    const digest = manifest.final();
    cmake_build.build_dir.path = try b.cache_root.join(b.allocator, &.{ "o", &digest });

    const cache_path = "o" ++ std.fs.path.sep_str ++ digest;
    var cache_dir = b.cache_root.handle.makeOpenPath(cache_path, .{}) catch |err| {
        return step.fail("unable to make path '{}{s}': {s}", .{
            b.cache_root, cache_path, @errorName(err),
        });
    };
    defer cache_dir.close();

    const sys_cmake = b.findProgram(&.{"cmake"}, &.{}) catch @panic("cmake not found");
    node.increaseEstimatedTotalItems(2);

    // cmake configure
    {
        var argv = std.ArrayList([]const u8).init(b.allocator);
        defer argv.deinit();

        try argv.append(sys_cmake);
        try argv.append("-S");
        try argv.append(cmake_build.opts.source_dir.getPath(b));
        try argv.append("-B");
        try argv.append(cmake_build.buildRoot().getPath(b));
        try argv.append("-G");
        try argv.append(cmake_build.opts.generator.toStr());

        const build_type_str = b.fmt("-DCMAKE_BUILD_TYPE={s}", .{@tagName(cmake_build.opts.build_type)});
        try argv.append(build_type_str);

        for (cmake_build.opts.options) |option| {
            const option_str = b.fmt("-D{s}={s}", .{ option[0], if (option[1]) "ON" else "OFF" });
            try argv.append(option_str);
        }

        var cmd = std.process.Child.init(try argv.toOwnedSlice(), b.allocator);
        cmd.stdout_behavior = .Ignore;
        cmd.stderr_behavior = .Ignore;

        cmake_build.step.name = b.fmt("cmake configure {s}", .{cmake_build.opts.name});
        try cmd.spawn();

        if (try cmd.wait() == .Exited) {
            node.completeOne();
        }
    }

    // cmake build
    {
        var argv = std.ArrayList([]const u8).init(b.allocator);
        defer argv.deinit();

        try argv.append(sys_cmake);
        try argv.append("--build");
        try argv.append(cmake_build.build_dir.getPath());
        if (cmake_build.opts.parallel) try argv.append("--parallel");

        const build_type_str = b.fmt("--config {s}", .{@tagName(cmake_build.opts.build_type)});
        try argv.append(build_type_str);

        var cmd_build = std.process.Child.init(try argv.toOwnedSlice(), b.allocator);
        cmd_build.stdout_behavior = .Ignore;
        cmd_build.stderr_behavior = .Ignore;

        cmake_build.step.name = b.fmt("cmake build {s}", .{cmake_build.opts.name});
        try cmd_build.spawn();

        if (try cmd_build.wait() == .Exited) {
            node.completeOne();
        }
    }
}
