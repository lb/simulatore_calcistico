# Simulatore Calcistico

## What is it
Simulatore Calcistico (SC) aims to be a FLOSS football game of the  
managerial / simulation kind.

## How to compile

SC uses the zig build system. It requires zig version 0.13.

To compile SC, invoke the build system like so
```sh
$ zig build
```

This will compile a debug version of SC.  
Dependencies are, at this moment, always compiled in ReleaseFast mode.

To compile a version of SC with a different optimization level,  
invoke the build system with the `-Doptimize` option, like so
```sh
$ zig build -Doptimize=ReleaseFast
```

The output binary will be placed in `./zig-out/bin/`.

It is also possible to run SC directly via the zig build system by  
running the `run` step, like so
```sh
$ zig build run
```

It is possible to provide flags to both the build system and SC itself  
without issue, like so
```sh
$ zig build -Doptimize=ReleaseFast run -- --run-tests
```

### Tooling
During the compilation phase, a very opinionated `clangd` file will be  
generated in the prefix specified to the build system (`./zig-out/` by  
default). My recommendation would be to create a symlink to this file  
in the root directory of SC, to allow clangd to work on the project.  
Note: the generated file is called `clangd`, but clangd expects to find  
a file called `.clangd` in a parent directory, so name your symlink
accordingly.

To customize the generated `clangd` file, change the options supplied to the  
`writeClangd.add()` function call in `build.zig`.

The fact that the `clangd` file is generated during the build process is  
subject to change in the future. `compile_commands.json` is possibly a better  
tool for the job, since the only info that needs to be generated are the  
include directories. Though, all SC files are compiled with the same flags  
so maybe only the dynamic part of the `clangd` will be generated as a fragment.

### Other build options
- in general, pass `--help` to the build command to see a list
- `-Dprofile=true` to enable profiling support via tracy [default off]
- `-Dtest=true` to enable testing support via doctest [default off]

### Behavioral and performance tests (AKA tests and benchmarks)
Enabling testing support does not mean that executing the compiled  
binary will then result in tests being run, it merely means that the  
tests are baked into the executable and can be invoked. In effect, it  
makes the executable a multipurpose one: run the tests or just SC.

To run tests, pass one of the `--run-tests`, `--run-benchmarks`,  
or `--run-suite`, or their short form equivalents `-t`, `-b`, or `-s`  
to the executable.

If requested to run tests, the executable will then exit without running SC.

Benchmarks are implemented as tests. Behavioral tests and benchmarks  
live under different test suites, which act as namespaces. Behavioral  
tests live in the `test` suite, while benchmarks live under the `bench`  
suite.

To filter test suites, pass the `--run-suite=<filter>` flag or its  
short form `-s=<filter>` to the executable. The filter is a comma  
separated list of filters.

Tests and benchmarks might be nested in sub suites. To see a list of  
suites that match a filter pass the `--list-suites` flag or its  
short form `-l` to the executable, along with the `-s=<filter>` flag.

Passing the `-l` flag to the executable without also providing a `-s=<filter>`  
flag will list every available suite.

Examples:
1. `$ ./zig-out/bin/sc` run SC
2. `$ ./zig-out/bin/sc -t` run behavioral tests only
3. `$ ./zig-out/bin/sc -s=test*` equivalent to the above
4. `$ ./zig-out/bin/sc -b` run benchmarks only
5. `$ ./zig-out/bin/sc -s=bench*` equivalent to the above
6. `$ ./zig-out/bin/sc -t -b` run both behavioral tests and benchmarks
7. `$ ./zig-out/bin/sc -s=test*,bench*` equivalent to the above
8. `$ ./zig-out/bin/sc -l -s=test*` list available behavioral test suites
9. `$ ./zig-out/bin/sc -l -s=bench*` list available benchmark suites
